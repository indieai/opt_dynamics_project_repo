# MIT License

MIT License

Copyright (c) 2016 Jason Jo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# Overview

This repository contains all the code and the tech report for the experiments performed in the [Exploring the Optimization Dynamics of Deep Convolutional Neural Networks](http://indieai.bitbucket.org/blog/2016/07/optimization-dynamics-rsph-lr-decay-resnet.html) project. 

All experiments were performed using the following setup:

* Ubuntu 14.04
* GeForce 960 4GB GPU, Intel i5-4570 CPU 3.2Ghz, 16 GB of RAM
* Python 2.7.6
* NumPy 1.10.4
* Theano 0.8.0.dev-1efb15394850a1cf08c7e26eea6fec256634aa7a
* Lasagne 0.2.dev1
* tsne 0.1.5
* matplotlib 1.5.0
* cuDNN 3007 (Major: 3, Minor: 0, Patch Level: 07)

The following datasets were used:

* [MNIST](http://deeplearning.net/data/mnist/mnist.pkl.gz)
* [CIFAR-10](https://www.cs.toronto.edu/~kriz/cifar.html)
* [Pentomino](http://lisaweb.iro.umontreal.ca/transfert/lisa/datasets/pentomino/datasets/)

**Warning:** When downloading the Pentomino datasets, please note that the pentomino_load function uses the original Pentomino filenames from the UMontreal link above. Please account for this when attempting to use this code. In particular in our pentomino_load function:

* Pentomino 10K dataset is called "pento64x64_10k_seed_23111298122_64patches.npy"
* Pentomino 20K dataset is called "pento64x64_20k_64patches_seed_112168712_64patches.pkl"
* Pentomino 40K dataset is called "pento64x64_40k_64patches_seed_975168712_64patches.npy"
* Pentomino 80K dataset is called "pento64x64_80k_64patches_seed_735128712_64patches.npy"

# Questions or Comments

For any questions or comments about this work, please contact me: [jason@indieai.com](mailto:jason@indieai.com)