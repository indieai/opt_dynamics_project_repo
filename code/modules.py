import theano, lasagne, os, time 
import theano.tensor as T
import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from collections import OrderedDict
from lasagne.layers import get_output, get_all_layers, get_all_params
from lasagne.layers import set_all_param_values, get_all_param_values
from lasagne.layers import get_output_shape, count_params
from lasagne.nonlinearities import softmax
from lasagne.objectives import categorical_crossentropy as cce
from lasagne.objectives import squared_error
from lasagne.regularization import regularize_layer_params, l2
from lasagne.updates import adadelta, momentum
from lasagne.utils import floatX
from utils import iterate_minibatches, iterate_minibatches_aux, random_crop

def adadelta_trainer(net, input_var, target_var, data_dict, save_iter, save_dir, num_epochs=100, batchsize=100, lr=1.0, rho=0.95, epsilon=1e-06, reg=0.0,test_batchsize=50, print_suppress=True):
	""" An Adadelta training function. Returns a dictionary with all the training diagnostics.

	Parameters
	----------

	* net : OrderedDict 
		An OrderedDict containing all the layers for the network. This network must be randomly initialized.

	* input_var : Theano symbolic variable
		A Theano symbolic variable representing the input to the network.

	* target_var : Theano symbolic variable 
		A Theano symbolic variable representing the targets of the network.

	* data_dict: OrderedDict
		An OrderedDict containing all the training and test data. Keys are: ['X_train', 'Y_train', 'X_test', 'Y_test'].

	* save_iter : int
		An integer specifying how often we will save the current parameters of the network. Default is set to 100.

	* save_dir : string 
		A string specifying the file directory to save the weights of the network.

	* num_epochs : int 
		An integer specifying the number of training epochs. Default is set to 100. 

	* batchsize : int 
		An integer specifying the size of each training batch. Default is set to 100.

	* test_batchsize: int 
		An integer specifying the size of each test batch. Default is set to 50.

	* train_continue : boolean True/False
		A boolean True/False which indicates whether or not we are continuing the training from a previous state or starting from a randomized initialization. Default is set to False. 

	* lr: float 
		A float specifying the learning rate. Default is set to 1.0.

	* rho: float 
		A float specifying the Adadelta decay coefficient. Default is set to 0.95.

	* epsilon : float 
		A float specifying an Adadelta numerical stability factor. Default is set to 1e-06.

	* reg : float 
		A float specifying the l2 regularization parameter value. Default is set to 0.0.

	* print_suppress : boolean True/False
		A boolean True/False which suppresses per-batch or per-save_iter printing. Default is set to True.
	"""
	# Unpack the data_dict:
	X_train = data_dict['X_train']
	Y_train = data_dict['Y_train']
	X_test = data_dict['X_test']
	Y_test = data_dict['Y_test']

	# Save the initialized weights:
	np.savez(os.path.join(save_dir, '0_iter_completed_weights.npz'), *get_all_param_values(net['softmax']))

	# Define output, loss, update, train_fn, test_fn
	if not print_suppress:
		print "Compiling training and test functions..."
	train_out = get_output(net['softmax'])
	train_loss = cce(train_out, target_var)
	train_loss = train_loss.mean()
	all_layers = get_all_layers(net['softmax'])
	reg_shared = theano.shared(floatX(reg))
	l2_penalty = reg * regularize_layer_params(all_layers, l2)
	train_loss = train_loss + l2_penalty 
	params = get_all_params(net['softmax'], trainable=True)
	lr_shared = theano.shared(floatX(lr))
	rho_shared = theano.shared(floatX(rho))
	epsilon_shared = theano.shared(floatX(epsilon))
	updates = adadelta(train_loss, params, learning_rate=lr_shared, rho=rho_shared, epsilon=epsilon_shared)
	train_fn = theano.function([input_var, target_var], train_loss, updates=updates)
	test_out = get_output(net['softmax'], deterministic=True)
	test_loss = cce(test_out, target_var)
	test_loss = test_loss.mean()
	test_acc = T.mean(T.eq(T.argmax(test_out, axis=1), target_var), dtype=theano.config.floatX)
	test_fn = theano.function([input_var, target_var], [test_loss, test_acc])

	# Define some training terms:
	num_train_batches = X_train.shape[0]/batchsize
	num_test_batches = X_test.shape[0]/test_batchsize
	max_iter = num_epochs*num_train_batches
	iter_count = 0
	test_count = 0

	train_loss_arr = np.zeros((max_iter))
	train_loss_epoch_arr = np.zeros((num_epochs))
	test_loss_arr = np.zeros((max_iter/save_iter))
	test_acc_arr = np.zeros((max_iter/save_iter))
	test_loss_epoch_arr = np.zeros((num_epochs))
	test_acc_epoch_arr = np.zeros((num_epochs))

	if not print_suppress:
		print "Officer Hoyt, today is your training day..."
	for i in xrange(num_epochs):
		train_loss_value = 0.0
		epoch_time = time.time()
		for inputs, targets in iterate_minibatches(X_train, Y_train, batchsize, shuffle=True):
			train_loss_arr[iter_count] = train_fn(inputs, targets)
			train_loss_value += train_loss_arr[iter_count]
			iter_count += 1
			if iter_count % save_iter == 0:
				# Save the params 
				np.savez(os.path.join(save_dir, '%d_iter_completed_weights.npz' % (iter_count)), *get_all_param_values(net['softmax']))

				# Evaluate on the test set:
				test_loss_value = 0
				test_acc_value = 0
				for inputs, targets in iterate_minibatches(X_test, Y_test, test_batchsize, shuffle=False):
					loss_val, acc = test_fn(inputs, targets)
					test_loss_value += loss_val
					test_acc_value += acc
				test_loss_arr[test_count] = test_loss_value/num_test_batches
				test_acc_arr[test_count] = test_acc_value/num_test_batches
				if not print_suppress:
					print "Training batch %d completed out of %d" % (iter_count, max_iter)
					print " train loss: \t%.6f" % (train_loss_arr[iter_count-1])
					print " test loss: \t%.6f" % (test_loss_arr[test_count]) 
					print " test acc: \t%.2f%%" % (test_acc_arr[test_count] * 100)
				test_count += 1
		# Test on the test set: 
		test_loss_value = 0
		test_acc_value = 0
		for inputs, targets in iterate_minibatches(X_test, Y_test, test_batchsize, shuffle=False):
			loss_val, acc = test_fn(inputs, targets)
			test_loss_value += loss_val
			test_acc_value += acc 
		test_loss_value /= float(num_test_batches)
		test_acc_value /= float(num_test_batches)

		train_loss_epoch_arr[i] = train_loss_value/float(num_train_batches)
		test_loss_epoch_arr[i] = test_loss_value
		test_acc_epoch_arr[i] = test_acc_value
		if not print_suppress:
			print "Completed epoch %d in %.6f seconds" % (i+1, time.time()-epoch_time)
			print "  train loss: \t%.6f" % (train_loss_epoch_arr[i])
			print "  test loss: \t%.6f" % (test_loss_epoch_arr[i])
			print "  test acc: \t%.2f%%" % (100*test_acc_epoch_arr[i])

	# Take a training snapshot:
	train_snapshot = OrderedDict()
	train_snapshot['iter_count'] = iter_count
	train_snapshot['epoch_count'] = num_epochs
	train_snapshot['test_count'] = test_count 
	train_snapshot['learning_rate'] = lr_shared.get_value()
	train_snapshot['rho'] = rho_shared.get_value()
	train_snapshot['epsilon'] = epsilon_shared.get_value()
	train_snapshot['reg'] = reg_shared.get_value()
	train_snapshot['train_loss_arr'] = train_loss_arr
	train_snapshot['train_loss_epoch_arr'] = train_loss_epoch_arr 
	train_snapshot['test_loss_arr'] = test_loss_arr
	train_snapshot['test_acc_arr'] = test_acc_arr
	train_snapshot['test_loss_epoch_arr'] = test_loss_epoch_arr
	train_snapshot['test_acc_epoch_arr'] = test_acc_epoch_arr

	pickle.dump(train_snapshot, open(os.path.join(save_dir, 'train_snapshot.pkl'), 'wb'))

	return train_snapshot

def momentum_snapshot_trainer(net, input_var, target_var, data_dict, save_iter, save_dir, num_epochs=85, batchsize=128, train_continue=False, train_snapshot=None, reg=0.0001, lr=0.1, mu=0.9, layer_wise_lr=False, layer_wise_lr_rates=None, test_batchsize=500, uniform_decay_dict=OrderedDict(((41,.1),(61,.1))), print_suppress=False):
	""" An SGD+momentum snapshot training function. Returns a dictionary with all the training diagnostics as well as all the information needed to continue training using the last network and momentum parameters.

	Parameters
	----------

	* net : OrderedDict 
		An OrderedDict containing all the layers for the network. This network must be randomly initialized.

	* input_var : Theano symbolic variable
		A Theano symbolic variable representing the input to the network.

	* target_var : Theano symbolic variable 
		A Theano symbolic variable representing the targets of the network.

	* data_dict: OrderedDict
		An OrderedDict containing all the training and test data. Keys are: ['X_train', 'Y_train', 'X_test', 'Y_test'].

	* save_iter : int
		An integer specifying how often we will save the current parameters of the network. 

	* save_dir : string 
		A string specifying the file directory to save the weights of the network.

	* num_epochs : int 
		An integer specifying the number of training epochs. Default is set to 85. 

	* batchsize : int 
		An integer specifying the size of each training batch. Default is set to 128.

	* test_batchsize: int 
		An integer specifying the size of each test batch. Default is set to 500.

	* train_continue : boolean True/False
		A boolean True/False which indicates whether or not we are continuing the training from a previous state or starting from a randomized initialization. Default is set to False. 

	* train_snapshot: dict 
		A dictionary containing a snapshot of where the previous training session left off. Will have the following keys: 
			* 'iter_count' : the number of training batches already processed
			* 'epoch_count' : the number of training epochs already completed
			* 'test_count' : the number of times we have evaluated on the test set
			* 'learning_rate' : the learning rate 
			* 'mu' : the momentum coefficient 
			* 'reg' : the l2 regularization coefficient 
			* 'saved_params' : the saved weight parameters 
			* 'saved_momentum' : a list of saved momentum velocity terms
			* 'train_loss_arr' : numpy array of all the loss values of the training batches
			* 'train_loss_epoch_arr' : numpy array of all the training loss values per epoch 
			* 'test_loss_arr' : numpy array of all the test loss values
			* 'test_acc_arr' : numpy array of all the test accuracies 

	* reg : float 
		A float specifying the l2 regularization parameter value. Default is set to 0.0001.

	* lr: float 
		A float specifying the learning rate. Default is set to 0.1.

	* mu: float 
		A float specifying the momentum coefficient. Default is set to 0.9.

	* layer_wise_lr: boolean True/False 
		A boolean True/False specifying whether or not to use a non-uniform layer-wise learning rate decay scheme.

	* layer_wise_lr_rates: tuple or list type 
		A tuple or list type specifying the non-uniform layer-wise learning rates.

	* uniform_decay_dict : OrderedDict
		An OrderedDict type where the keys are the epoch numbers to decay the learning rate and the corresponding values are the lr decay rates. Default is set to 41st and 61st epochs with a uniform decay rate of 0.1 at each epoch. 

	* print_suppress : boolean True/False
		A boolean True/False which suppresses per-batch or per-save_iter printing. Default is set to True.

	"""
	# Unpack the data_dict:
	X_train = data_dict['X_train']
	Y_train = data_dict['Y_train']
	X_test = data_dict['X_test']
	Y_test = data_dict['Y_test']

	if not train_continue:
		# Save the initialized weights:
		np.savez(os.path.join(save_dir, '0_iter_completed_weights.npz'), *get_all_param_values(net['softmax']))

	# Define output, loss, update, train_fn, test_fn
	if print_suppress == False:
		print "Compiling training and test functions..."
	train_out = get_output(net['softmax'])
	train_loss = cce(train_out, target_var)
	train_loss = train_loss.mean()
	all_layers = get_all_layers(net['softmax'])
	reg_shared = theano.shared(floatX(reg))
	l2_penalty = reg * regularize_layer_params(all_layers, l2)
	train_loss = train_loss + l2_penalty 
	params = get_all_params(net['softmax'], trainable=True)
	lr_shared = theano.shared(floatX(lr))
	momentum_shared = theano.shared(floatX(mu))
	if not layer_wise_lr:
		updates = OrderedDict()
		trainable_layer_idx = 0
		for layer in all_layers:
			trainable_params = layer.get_params(trainable=True)
			if len(trainable_params) > 0:
				updates.update(momentum(train_loss, trainable_params, lr_shared, momentum_shared))
				trainable_layer_idx += 1
	else:
		updates = OrderedDict()
		trainable_layer_idx = 0
		for layer in all_layers:
			trainable_params = layer.get_params(trainable=True)
			if len(trainable_params) > 0:
				updates.update(momentum(train_loss, trainable_params, layer_wise_lr_rates[trainable_layer_idx], momentum_shared))
				trainable_layer_idx += 1
	train_fn = theano.function([input_var, target_var], train_loss, updates=updates)

	test_out = get_output(net['softmax'], deterministic=True)
	test_loss = cce(test_out, target_var)
	test_loss = test_loss.mean()
	test_acc = T.mean(T.eq(T.argmax(test_out, axis=1), target_var), dtype=theano.config.floatX)
	test_fn = theano.function([input_var, target_var], [test_loss, test_acc])

	# Define some training terms:
	epoch_start = 0
	num_train_batches = X_train.shape[0]/batchsize
	num_test_batches = X_test.shape[0]/test_batchsize
	max_iter = num_epochs*num_train_batches
	iter_count = 0
	test_count = 0

	# If we are continuing from a previous training session, we load all the relevant saved terms: 
	if train_continue:
		print "Reloading a snapshot of a previous training state..."
		# Set the learning rate:
		lr = train_snapshot['learning_rate']
		lr_shared.set_value(floatX(lr))

		# Set the momentum:
		mu = train_snapshot['mu']
		momentum_shared.set_value(floatX(mu))

		# Set the reg:
		reg = train_snapshot['reg']
		reg_shared.set_value(floatX(reg))

		# Load the previously saved params: 
		saved_params = train_snapshot['saved_params']
		set_all_param_values(net['softmax'], saved_params)

		# Load the saved momentum terms: 
		saved_momentum = train_snapshot['saved_momentum']
		saved_momentum_index = 0
		for index, update in enumerate(updates):
			# Here we want to update the velocity terms. The nonvelocity terms in the momentum updates will have their name module of type string, while the velocity terms have name type None. So in order to loop through the velocity terms we do the following:
			if type(update.name) != str: 
				new_velocity = saved_momentum[saved_momentum_index]
				updates.items()[index][0].get_value(new_velocity)
				saved_momentum_index += 1

		# Set the iter_count:
		iter_count = train_snapshot['iter_count']

		# Set the epoch_start:
		epoch_start = train_snapshot['epoch_count']

		# Set the test_count:
		test_count = train_snapshot['test_count']

		# Update max_iter:
		max_iter += epoch_start * num_train_batches

		# Create training diagnostic arrays:
		total_epochs = epoch_start+num_epochs
		train_loss_arr = np.zeros((max_iter))
		train_loss_epoch_arr = np.zeros((total_epochs))
		test_loss_arr = np.zeros((max_iter/save_iter))
		test_acc_arr = np.zeros((max_iter/save_iter))
		test_loss_epoch_arr = np.zeros((total_epochs))
		test_acc_epoch_arr = np.zeros((total_epochs))

		# Concatenate all the training diagnostic arrays:
		train_loss_arr[:iter_count] = train_snapshot['train_loss_arr']
		train_loss_epoch_arr[:epoch_start] = train_snapshot['train_loss_epoch_arr']
		test_loss_arr[:test_count] = train_snapshot['test_loss_arr']
		test_acc_arr[:test_count] = train_snapshot['test_acc_arr']
		test_loss_epoch_arr[:epoch_start] = train_snapshot['test_loss_epoch_arr']
		test_acc_epoch_arr[:epoch_start] = train_snapshot['test_acc_epoch_arr']
	else:
		train_loss_arr = np.zeros((max_iter))
		train_loss_epoch_arr = np.zeros((num_epochs))
		test_loss_arr = np.zeros((max_iter/save_iter))
		test_acc_arr = np.zeros((max_iter/save_iter))
		test_loss_epoch_arr = np.zeros((num_epochs))
		test_acc_epoch_arr = np.zeros((num_epochs))

	if print_suppress == False:
		print "Officer Hoyt, today is your training day..."
	for i in xrange(epoch_start, epoch_start+num_epochs):
		if i in uniform_decay_dict:
			# Decay the learning rate:
			lr_decay_rate = uniform_decay_dict[i]
			lr_shared.set_value(np.array(lr_shared.get_value()*lr_decay_rate, dtype=theano.config.floatX)) 
			if print_suppress == False:
				print "  decaying the learning rate @ epoch %d, training batch %d, new lr: %.6f" % (i+1, iter_count, lr_shared.get_value())
		train_loss_value = 0
		epoch_time = time.time()
		for inputs, targets in iterate_minibatches(X_train, Y_train, batchsize, shuffle=True):
			inputs = random_crop(inputs, crop_size=(32,32), pad=(4,4))
			train_loss_arr[iter_count] = train_fn(inputs, targets)
			train_loss_value += train_loss_arr[iter_count]
			iter_count += 1
			if iter_count % save_iter == 0:
				# Save the params 
				np.savez(os.path.join(save_dir, '%d_iter_completed_weights.npz' % (iter_count)), *get_all_param_values(net['softmax']))

				# Evaluate on the test set:
				test_loss_value = 0
				test_acc_value = 0
				for inputs, targets in iterate_minibatches(X_test, Y_test, test_batchsize, shuffle=False):
					loss_val, acc = test_fn(inputs, targets)
					test_loss_value += loss_val
					test_acc_value += acc
				test_loss_arr[test_count] = test_loss_value/num_test_batches
				test_acc_arr[test_count] = test_acc_value/num_test_batches
				if print_suppress == False:
					print "Training batch %d completed out of %d" % (iter_count, max_iter)
					print "train loss: \t%.6f" % (train_loss_arr[iter_count-1])
					print " test loss: \t%.6f" % (test_loss_arr[test_count]) 
					print " test acc: \t%.2f%%" % (test_acc_arr[test_count] * 100)
				test_count += 1
		# Test on the test set: 
		test_loss_value = 0
		test_acc_value = 0
		for inputs, targets in iterate_minibatches(X_test, Y_test, test_batchsize, shuffle=False):
			loss_val, acc = test_fn(inputs, targets)
			test_loss_value += loss_val
			test_acc_value += acc 
		test_loss_value /= float(num_test_batches)
		test_acc_value /= float(num_test_batches)

		train_loss_epoch_arr[i] = train_loss_value/float(num_train_batches)
		test_loss_epoch_arr[i] = test_loss_value
		test_acc_epoch_arr[i] = test_acc_value
		if print_suppress == False:
			print "Completed epoch %d in %.6f seconds" % (i+1, time.time()-epoch_time)
			print "  train loss: \t%.6f" % (train_loss_epoch_arr[i])
			print "  test loss: \t%.6f" % (test_loss_epoch_arr[i])
			print "  test acc: \t%.2f%%" % (100*test_acc_epoch_arr[i])

	# Take a training snapshot:
	train_snapshot = OrderedDict()
	train_snapshot['iter_count'] = iter_count
	train_snapshot['epoch_count'] = epoch_start+num_epochs
	train_snapshot['test_count'] = test_count 
	train_snapshot['learning_rate'] = lr_shared.get_value()
	train_snapshot['mu'] = momentum_shared.get_value()
	train_snapshot['reg'] = reg_shared.get_value()
	train_snapshot['saved_params'] = get_all_param_values(net['softmax'])
	saved_momentum = []
	# Cache the velocity terms only:
	for index, update in enumerate(updates):
		if type(update.name) != str:
			saved_momentum.append(updates.items()[index][0].get_value())
	train_snapshot['saved_momentum'] = saved_momentum
	train_snapshot['train_loss_arr'] = train_loss_arr
	train_snapshot['train_loss_epoch_arr'] = train_loss_epoch_arr 
	train_snapshot['test_loss_arr'] = test_loss_arr
	train_snapshot['test_acc_arr'] = test_acc_arr
	train_snapshot['test_loss_epoch_arr'] = test_loss_epoch_arr
	train_snapshot['test_acc_epoch_arr'] = test_acc_epoch_arr

	pickle.dump(train_snapshot, open(os.path.join(save_dir, 'train_snapshot.pkl'), 'wb'))

	return train_snapshot

def compute_activation_statistics(x_init, x_final, delta=1e-6):
	""" Computes the activation statistics for activation outputs from two different training instances. Will return an OrderedDict with the following keys and values: 

		* 'total_units' : an integer value which represents the total number of possible activations. 

		* 'deactivate_agree' : a float value representing the percentage of all units that were deactviated for both sets of network parameters.

		* 'deactivate_new' : a float value representing the percentage of units which were activated for the init_params forward pass but deactivated for the final_params forward pass. 

		* 'activate_new' : a float value representing the percentage of units whch were deactivated for the init_params forward pass but activated for the final params forward pass.

		* 'activate_agree' : a float value representing the percentage of units which were activated by both the init_params and final_params forward passes. 

	Parameters
	----------

	* x_init : numpy array 
		A 4D numpy array which is the output of a layer in a deep network using init params.

	* x_final : numpy array
		A 4D numpy array which is the output of a layer in a deep network using final params.

	* delta : float 
		A thresholding parameter that will convert all activations values < delta to be zero. Default is set to 1e-6. 
	"""
	# Define the shape dimensions of the batch output:
	n, c, h, w = x_init.shape
	total_units = n*c*h*w

	# Assert that x_init and x_final have the same shape:
	pass 

	# Initialize activation statistics:
	deactivate_agree = 0
	deactivate_new = 0
	activate_new = 0
	activate_agree = 0

	# Threshold the activation statistics:
	x_init[x_init < delta] = 0
	x_final[x_final < delta] = 0

	deactivate_agree = np.sum((x_init == 0) & (x_final == 0))
	deactivate_new = np.sum((x_init > 0) & (x_final == 0))
	activate_new = np.sum((x_init == 0) & (x_final > 0))
	activate_agree = np.sum((x_init > 0) & (x_final > 0))

	# Create an OrderedDict to store all the activation statistics:
	stat_dict = OrderedDict() 
	stat_dict['total_units'] = total_units
	stat_dict['deactivate_agree'] = deactivate_agree
	stat_dict['deactivate_new'] = deactivate_new
	stat_dict['activate_new'] = activate_new
	stat_dict['activate_agree'] = activate_agree

	return stat_dict

def train_mimic_net(save_dir, student_net, input_var, target_var, data_dict, num_epochs, update_mode, batchsize=128, test_batchsize=500, reg=0.0, lr=0.001, mu=0.9, num_classes=10, save_iter=100, decay_dict=OrderedDict(((41,.1),(61,.1))), train_continue=False, train_snapshot=None):
	""" This function will train a student net to mimic a teacher net. The target that the student net will learn to mimic are the teacher nets' logits. Training will be done using SGD+Momentum. Returns a dictionary with all the training diagnostics.

	Parameters
	----------

	* save_dir : string 
		A string specifying the file directory to save the weights of the network.

	* student_net : OrderedDict 
		An OrderedDict containing all the layers for the student_net 

	* input_var : Theano symbolic variable
		A Theano symbolic variable representing the input to the network.

	* target_var : Theano symbolic variable 
		A Theano symbolic variable representing the class label targets of the network.

	* data_dict: OrderedDict
		An OrderedDict containing all the training and test data. Keys are: ['X_train', 'Y_train', 'Y_train_logits', 'X_test', 'Y_test', 'Y_test_logits'].

	* num_epochs : int 
		An integer specifying the number of epochs to train for. 

	* update_mode : string
		A string indicating the update mode: 'momentum' or 'adadelta'.

	* batchsize : int 
		An integer specifying the size of each training batch. Default is set to 128.

	* test_batchsize: int 
		An integer specifying the size of each test batch. Default is set to 500.

	* reg : float 
		A float specifying the l2 regularization parameter value. Default is set to 0.0.

	* lr: float 
		A float specifying the learning rate. Default is set to 0.001.

	* mu: float 
		A float specifying the momentum coefficient. Default is set to .9.

	* num_classes: int 
		An integer specifying the number of classes.

	* save_iter : int
		An integer specifying how often we will save the current parameters of the network. Default is set to 100.

	* decay_dict : OrderedDict
		An OrderedDict type where the keys are the epoch numbers to decay the learning rate and the corresponding values are the lr decay rates. Default is set to 41st and 61st epochs with a uniform decay rate of 0.1 at each epoch. 

	"""
	# Unpack the data_dict:
	X_train = data_dict['X_train']
	Y_train = data_dict['Y_train']
	Y_train_logits = data_dict['Y_train_logits']
	X_test = data_dict['X_test']
	Y_test = data_dict['Y_test']
	Y_test_logits = data_dict['Y_test_logits']

	# Create student_net logit L2 loss expressions: 
	print "Compiling student_net training and test functions..."
	student_target_var = T.fmatrix('logit targets')
	student_logits = get_output(student_net['logits'])
	student_l2_loss = squared_error(student_logits, student_target_var)
	student_l2_loss = student_l2_loss.mean()
	all_layers = get_all_layers(student_net['logits'])
	l2_penalty = regularize_layer_params(all_layers, l2)*reg
	student_l2_loss = student_l2_loss + l2_penalty 

	# Create student_net softmax loss expressions:
	student_softmax = get_output(student_net['softmax'])
	student_softmax_loss = cce(student_softmax, target_var)
	student_softmax_loss = student_softmax_loss.mean()
	student_softmax_loss = student_softmax_loss + l2_penalty 

	# Compile a training function: 
	params = get_all_params(student_net['logits'], trainable=True)
	lr_shared = theano.shared(floatX(lr))
	if update_mode == 'momentum':
		print "Using momentum updates..."
		updates = momentum(student_l2_loss, params, learning_rate=lr_shared, momentum=mu)
	elif update_mode == 'adadelta':
		print "Using Adadelta updates..."
		updates = adadelta(student_l2_loss, params, learning_rate=1.0, rho=0.95, epsilon=1e-06)
	train_fn = theano.function([input_var, student_target_var, target_var], [student_l2_loss, student_softmax_loss], updates=updates)

	# Create student_net test logit loss expressions:
	test_logit_out = get_output(student_net['logits'], deterministic=True)
	test_l2_loss = squared_error(test_logit_out, student_target_var)
	test_l2_loss = test_l2_loss.mean()

	# Create student_net test softmax loss expressions:
	test_softmax_out = get_output(student_net['softmax'], deterministic=True)
	test_softmax_loss = cce(test_softmax_out, target_var)
	test_softmax_loss = test_softmax_loss.mean()

	# Compile a testing function:
	test_acc = T.mean(T.eq(target_var, T.argmax(test_softmax_out, axis=1)), dtype=theano.config.floatX)
	test_fn = theano.function([input_var, student_target_var, target_var], [test_l2_loss, test_softmax_loss, test_acc])

	# Define some training diagnostics:
	num_train_batches = X_train.shape[0]/batchsize 
	num_test_batches = X_test.shape[0]/test_batchsize
	max_iter = num_train_batches*num_epochs 
	iter_count = 0
	test_count = 0

	train_l2_loss_arr = np.zeros((max_iter))
	train_l2_loss_epoch_arr = np.zeros((num_epochs))
	train_softmax_loss_arr = np.zeros((max_iter))
	train_softmax_loss_epoch_arr = np.zeros((num_epochs))
	test_l2_loss_epoch_arr = np.zeros((num_epochs))
	test_l2_loss_arr = np.zeros((max_iter/save_iter))
	test_softmax_loss_epoch_arr = np.zeros((num_epochs))
	test_softmax_loss_arr = np.zeros((max_iter/save_iter))
	test_acc_epoch_arr = np.zeros((num_epochs))
	test_acc_arr = np.zeros((max_iter/save_iter))

	print "Officer Hoyt, today is your training day..."
	for i in xrange(num_epochs):

		if i+1 in decay_dict:
			# Decay the learning rate:
			lr_decay_rate = decay_dict[i+1]
			lr_shared.set_value(np.array(lr_shared.get_value()*lr_decay_rate, dtype=theano.config.floatX)) 
			print "  decaying the learning rate @ epoch %d, training batch %d, new lr: %.6f" % (i+1, iter_count, lr_shared.get_value())

		train_l2_loss, train_softmax_loss = 0, 0
		epoch_time = time.time()
		for inputs, targets, aux_targets in iterate_minibatches_aux(X_train, Y_train_logits, Y_train, batchsize=batchsize, shuffle=True):
			inputs = random_crop(inputs, crop_size=(32,32), pad=(4,4))
			batch_l2, batch_softmax = train_fn(inputs, targets, aux_targets)
			train_l2_loss += batch_l2
			train_softmax_loss += batch_softmax
			train_l2_loss_arr[iter_count] = batch_l2
			train_softmax_loss_arr[iter_count] = batch_softmax			
			iter_count += 1

			if iter_count % save_iter == 0:
				# Save the weights:
				np.savez(save_dir+'%d_iter_completed_weights.npz' % (iter_count), *get_all_param_values(student_net['logits']))

				# Test on the test set: 
				test_l2_loss, test_softmax_loss, test_acc = 0, 0, 0
				for inputs, targets, aux_targets in iterate_minibatches_aux(X_test, Y_test_logits, Y_test, batchsize=test_batchsize, shuffle=False):
					batch_l2, batch_softmax, batch_acc = test_fn(inputs, targets, aux_targets)
					test_l2_loss += batch_l2
					test_softmax_loss += batch_softmax
					test_acc += batch_acc 
				test_l2_loss_arr[test_count] = test_l2_loss/num_test_batches
				test_softmax_loss_arr[test_count] = test_softmax_loss/num_test_batches
				test_acc_arr[test_count] = test_acc/num_test_batches
				test_count += 1

		train_l2_loss_epoch_arr[i] = train_l2_loss/float(num_train_batches)
		train_softmax_loss_epoch_arr[i] = train_softmax_loss/float(num_train_batches)
		epoch_time = time.time() - epoch_time
		
		# Test on the test set: 
		test_l2_loss, test_softmax_loss, test_acc = 0, 0, 0
		for inputs, targets, aux_targets in iterate_minibatches_aux(X_test, Y_test_logits, Y_test, batchsize=test_batchsize, shuffle=False):
			batch_l2, batch_softmax, batch_acc = test_fn(inputs, targets, aux_targets)
			test_l2_loss += batch_l2
			test_softmax_loss += batch_softmax
			test_acc += batch_acc 
		test_l2_loss_epoch_arr[i] = test_l2_loss/float(num_test_batches)
		test_softmax_loss_epoch_arr[i] = test_softmax_loss/float(num_test_batches)
		test_acc_epoch_arr[i] = test_acc/float(num_test_batches)

		print "Completed epoch %d in %.6f seconds" % (i+1, epoch_time)
		print "	train l2 loss: \t\t%.6f" % (train_l2_loss_epoch_arr[i])
		print "	train softmax loss: \t%.6f" % (train_softmax_loss_epoch_arr[i])
		print "	test l2 loss: \t\t%.6f" % (test_l2_loss_epoch_arr[i])
		print "	test softmax loss: \t%.6f" % (test_softmax_loss_epoch_arr[i])
		print "	test acc: \t\t%.2f%%" % (test_acc_epoch_arr[i]*100)

	# Save all the training diagnostics as an OrderedDict:
	training_diagnostics = OrderedDict()
	training_diagnostics['train l2 per batch'] = train_l2_loss_arr
	training_diagnostics['train softmax per batch'] = train_softmax_loss_arr
	training_diagnostics['train l2 per epoch'] = train_l2_loss_epoch_arr
	training_diagnostics['train softmax per epoch'] = train_softmax_loss_epoch_arr
	training_diagnostics['test l2 per epoch'] = test_l2_loss_epoch_arr
	training_diagnostics['test l2 arr'] = test_l2_loss_arr
	training_diagnostics['test softmax per epoch'] = test_softmax_loss_epoch_arr
	training_diagnostics['test softmax arr'] = test_softmax_loss_arr
	training_diagnostics['test acc per epoch'] = test_acc_epoch_arr
	training_diagnostics['test acc arr'] = test_acc_arr

	pickle.dump(training_diagnostics, open(os.path.join(save_dir, 'training_diagnostics.pkl'), 'wb'))

	return training_diagnostics

if __name__ == '__main__':
	pass 