import theano, lasagne, os, time, sys
import theano.tensor as T
import numpy as np
import cPickle as pickle
from collections import OrderedDict
from lasagne.layers import set_all_param_values, get_output
from lasagne.objectives import categorical_crossentropy as cce
from utils import gcn, flips, random_crop, iterate_minibatches
from data_load import pentomino_load
from networks import build_pentomino_deep_vgg_resnet

""" A ResNet-3-S2 model which attains 99.975% test accuracy on the 40K Pentomino dataset with a 32K/8K train/test split.
"""

# Initialize a ResNet-3-S2 network:
input_var = T.tensor4('inputs')
target_var = T.ivector('targets')
resnet_3_s2 = build_pentomino_deep_vgg_resnet(input_var=input_var, stack_depth=3, first_stride=2)

# Load the best performing weights:
with np.load(os.path.join(sys.path[0], 'resnet_3_s2_9997_test_acc.npz')) as f:
	resnet_best_params = [f['arr_%d' % j] for j in range(len(f.files))]
set_all_param_values(resnet_3_s2['softmax'], resnet_best_params)

# Define Theano symbolic test expressions and a Theano test_fn function:
test_out = get_output(resnet_3_s2['softmax'], deterministic=True)
test_loss = cce(test_out, target_var)
test_loss = test_loss.mean()
test_acc = T.mean(T.eq(T.argmax(test_out, axis=1), target_var), dtype=theano.config.floatX)
test_fn = theano.function([input_var, target_var], [test_loss, test_acc])

# Load the Pentomino dataset: 
pentomino_dir = '/path_to_pentomino_dataset/'
X_train, Y_train, X_val, Y_val, X_test, Y_test = pentomino_load(mode='40k', num_train=32000, num_val=0, dir_path=pentomino_dir)
X_train, X_val, X_test, mean_img = gcn(X_train, X_val, X_test, False)

test_loss_value = 0.0
test_acc_value = 0.0
test_batchsize = 500
num_test_batches = 8000/test_batchsize

for inputs, targets in iterate_minibatches(X_test, Y_test, test_batchsize, shuffle=False):
	loss_val, acc = test_fn(inputs, targets)
	test_loss_value += loss_val
	test_acc_value += acc

test_loss_value /= float(num_test_batches)
test_acc_value /= float(num_test_batches)

print "Test loss: \t%f" % (test_loss_value)
print "Test accuracy: \t%.4f%%" % (100*test_acc_value)