import theano
import theano.tensor as T
from collections import OrderedDict 
from lasagne.layers import InputLayer, Conv2DLayer, BatchNormLayer
from lasagne.layers import NonlinearityLayer, ConcatLayer, ExpressionLayer
from lasagne.layers import PadLayer, ElemwiseSumLayer, GlobalPoolLayer
from lasagne.layers import DenseLayer, MaxPool2DLayer, dropout
from lasagne.nonlinearities import rectify, linear, softmax 
from lasagne.init import Normal, HeNormal, GlorotUniform

###############################################################################
################################## Modules ####################################
###############################################################################

def build_conv_bn_layer(incoming, name, num_filters, filter_size, stride, pad, W_init, nonlinearity=rectify):
	""" Constructs a Conv2D layer with batch normalization applied before the nonlinearity. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* num_filters : int 
		An integer specifying the number of convolutional filters this layer has.

	* filter_size : int
		An integer specifying the receptive field size of each of the filters.

	* stride : int
		An integer specifying the stride of the convolution operation.

	* pad : int, or 'full' or 'same' or 'valid'
		An integer which specifies the amount of padding to use. See Lasagne documentation on Conv2DLayers for further reference. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function. let name = 'conv1'. This function returns an OrderedDict with the following keys:

		(1) 'conv1.conv' : the raw output of the convolutional layer.
		(2) 'conv1.bn' : the output of the batch normalization layer. 
		(3) 'conv1.nonlinearity' : the output after we apply the nonlinearity 					 	 to the batch normalization output.
	"""

	out = OrderedDict()

	# List of all the keys in 'name.layer' format, we do this for convenience and readability of code. 
	keys = ['{}.{}'.format(name,'conv'),
			'{}.{}'.format(name,'bn'), 
			'{}.{}'.format(name,'nonlinearity')] 

	out[keys[0]] = Conv2DLayer(	incoming=incoming, 
								num_filters=num_filters,
								filter_size=filter_size,
								stride=stride,
								pad=pad,
								W=W_init,
								b=None,
								nonlinearity=linear)

	out[keys[1]] = BatchNormLayer(out[keys[0]])

	out[keys[2]] = NonlinearityLayer(incoming=out[keys[1]], 
									 nonlinearity=nonlinearity)

	return out

def build_multi_scale_inception_bn_layer(incoming, name, num_filters, filter_sizes, strides, pads, W_init, nonlinearity=rectify):
	""" Implements a version of the Google Research Inception architecture: constructs a layer which is a concatenation of Conv-BN modules of differing receptive field sizes. We do not include max-pooling layers in our Inception modules. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* num_filters : list of ints
		A list of integers specifying the number of convolutional filters for each scale.

	* filter_sizes : list of ints
		A list of integers specifying the receptive field sizes of each convolutional layer, e.g. the different spatial scales. 

		Example: [3,5] would correspond to an Inception layer which is the concatenation of two different convolutional layers, one with receptive field size (3,3) and the other with receptive field size (5,5).

	* strides : list of ints, or list of iterable of int
		A list of ints or a list of 2-tuples of ints specifying the stride of each of the convolution operations.

	* pads : list of ints, or iterables of int, 'full', 'same' or 'valid'
		A list of integers specifying the padding for each of the convolutional layers.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let name = 'Inception1' and filter_sizes=[3,5], so this is an Inception layer which will be the concatenation of a 3x3 conv layer and a 5x5 conv layer. This function will return an OrderedDict with the following keys:

		(1) 'Inception1.conv3.conv' : the raw output of the 3x3 convolutional 							layer.

		(2) 'Inception1.conv3.bn'	: the output of the batch normalization 						  layer of the conv-3x3 output.

		(3) 'Inception1.conv3.nonlinearity' : the output after we apply the 								  nonlinearity to the batch 									  normalized conv-3x3 output.

		(4) 'Inception1.conv5.conv' : the raw output of the 5x5 convolutional 							layer.

		(5) 'Inception1.conv5.bn'	: the output of the batch normalization 						  layer of the conv-5x5 output .

		(6) 'Inception1.conv5.nonlinearity' : the output after we apply the 								  nonlinearity to the batch 									  normalized conv-5x5 output

		(7)	'Inception1.concat' : the concatenation of 	the two layers							  'Inception1.conv3.nonlinearity' and 							  'Inception1.conv3.nonlinearity'


	Reference
	---------
	Title: 		'Going Deeper with Convolutions'

	Authors: 	Christian Szegedy, Wei Liu, Yangqing Jia, Pierre Sermanet, 
				Scott Reed, Dragomir Anguelov, Dumitru Erhan, 
				Vincent Vanhoucke, Andrew Rabinovich

	arXiv: 		http://arxiv.org/abs/1409.4842

	"""
	# Make sure that the size of the parameter lists all match:
	assert (len(num_filters)==len(filter_sizes)==len(strides)==len(pads)), "Number of Inception Layers do not match. Check to make sure the length of the num_filters, filter_size, stride, pad lists all agree."

	out = OrderedDict()

	num_layers = len(num_filters)

	# Create each of the convolutional layers:
	for i in xrange(num_layers):
		out.update(
			build_conv_bn_layer(
				incoming=incoming, 
				name='{}.{}'.format(name, 'conv'+str(filter_sizes[i])), 
				num_filters=num_filters[i], 
				filter_size=filter_sizes[i], 
				stride=strides[i], 
				pad=pads[i], 
				W_init=W_init, 
				nonlinearity=rectify))

	# Concatenate all the outputs
	out['{}.{}'.format(name, 'concat')] = ConcatLayer([out['{}.{}.{}'.format(name, 'conv'+str(filter_sizes[i]), 'nonlinearity')] for i in xrange(num_layers)])

	return out 

def build_fc_bn_module(incoming, name, num_units, W_init, nonlinearity=rectify):
	""" Constructs a dense layer with batch normalization applied before the nonlinearity. 

	Parameters
	----------
	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* num_units : int 
		An integer specifying the number of neuronal units this layer has.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	"""

	out = OrderedDict()

	keys = ['{}.{}'.format(name, 'fc'),
			'{}.{}'.format(name, 'bn'),
			'{}.{}'.format(name, 'nonlinearity')
			]

	out[keys[0]] 	=	DenseLayer(
											incoming=incoming,
											num_units=num_units,
											W=W_init,
											b=None,
											nonlinearity=None)

	out[keys[1]]	=	BatchNormLayer(out[keys[0]])

	out[keys[2]] 	= NonlinearityLayer(incoming=out[keys[1]], 
										nonlinearity=nonlinearity)

	return out

def build_vgg_residual_block(incoming, name, filter_size, increase_filters, projection, W_init, nonlinearity=rectify):
	""" Implements a version of the Deep Residual Blocks. All convolutional layers in this residual block have the same receptive field size.  

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* filter_size : int 
		An integer specifying the receptive field size of each of the filters.

	* increase_filters : boolean
		True/False indicating whether or not we are cutting the spatial size in half (N,N) -> (N/2, N/2) and doubling the number of convolutional filters. 

	* projection: boolean 
		True/False indicating if we are using identity skip connections or projection skip connections. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let name = 'resblock1_1', num_layers = 2, filter_size = 3, increase_filters = True and projection = False. This function will return an OrderedDict with the following keys:

		(1)  'resblock1_1.conv1.conv'
		(2)  'resblock1_1.conv1.bn'
		(3)  'resblock1_1.conv1.nonlinearity'
		(4)  'resblock1_1.conv2.conv'
		(5)  'resblock1_1.conv2.bn'
		(6)  'resblock1_1.conv2.nonlinearity'
		(7)  'resblock1_1.subsample'
		(8)  'resblock1_1.padded'
		(9)	 'resblock1_1.elemwisesum'
		(10) 'resblock1_1.residual'

	If instead we had that projection = True: 

		(1)  'resblock1_1.conv1.conv'
		(2)  'resblock1_1.conv1.bn'
		(3)  'resblock1_1.conv1.nonlinearity'
		(4)  'resblock1_1.conv2.conv'
		(5)  'resblock1_1.conv2.bn'
		(6)  'resblock1_1.conv2.nonlinearty'
		(7)  'resblock1_1.projection.conv'
		(8)  'resblock1_1.projection.bn'
		(9)	 'resblock1_1.projection.nonlinearity'
		(10) 'resblock1_1.elemwisesum'
		(11) 'resblock1_1.residual'

	Reference
	---------
	Title:		Deep Residual Learning for Image Recognition

	Authors: 	Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun

	arXiv:		http://arxiv.org/abs/1512.03385

	"""
	out = OrderedDict()

	num_input_filters = incoming.output_shape[1]

	if increase_filters:
		first_stride = 2
		num_output_filters = 2 * num_input_filters
	else:
		first_stride = 1
		num_output_filters = num_input_filters

	out.update(	build_conv_bn_layer(
						incoming=incoming, 
						name='{}.{}'.format(name,'conv1'), 
						num_filters=num_output_filters, 
						filter_size=filter_size, 
						stride=first_stride, 
						pad='same', 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	out.update(	build_conv_bn_layer(
						incoming=out['{}.{}.{}'.format(name,'conv1', 'nonlinearity')], 
						name='{}.{}'.format(name,'conv2'), 
						num_filters=num_output_filters, 
						filter_size=filter_size, 
						stride=1, 
						pad='same', 
						W_init=W_init, 
						nonlinearity=linear))

	if increase_filters:
		if projection:
			# Perform projection via Option B in the MSR Deep ResNet article, 
			# i.e. via convolution with spatial stride 2, 2x filter increase:
			out.update(	build_conv_bn_layer(
					incoming=incoming, 
					name='{}.{}'.format(name,'projection'), 
					num_filters=num_output_filters, 
					filter_size=1, 
					stride=first_stride, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity)) # Linear or rectify?

			keys = ['{}.{}.{}'.format(name,'projection','nonlinearity'), 
					'{}.{}.{}'.format(name,'conv2','nonlinearity'), 
					'{}.{}'.format(name, 'elemwisesum'),
					'{}.{}'.format(name,'residual')]

			out[keys[2]]	=	ElemwiseSumLayer([out[keys[0]], out[keys[1]]])

			out[keys[-1]] 	=	NonlinearityLayer(out[keys[2]], nonlinearity=nonlinearity)
		else:
			# Perform projection via Option A in the MSR Deep ResNet article
			# i.e. via spatial subsampling and filter dimension zero padding

			keys = ['{}.{}'.format(name,'subsample'), 
					'{}.{}'.format(name,'padded'), 
					'{}.{}.{}'.format(name,'conv2','nonlinearity'), 
					'{}.{}'.format(name, 'elemwisesum'),
					'{}.{}'.format(name,'residual')]

			# Subsample the spatial dimensions to decrease the spatial dimensionality
			out[keys[0]] 	=	ExpressionLayer(	
									incoming=incoming,
									function=lambda X: X[:,:,::2,::2],
									output_shape=lambda s: (s[0], s[1], s[2]//2, s[3]//2))

			# We pad the inputs filter dimension with zeros
			out[keys[1]] 	= 	PadLayer(	
									incoming=out[keys[0]],
									width=[num_output_filters//4,0,0],
									batch_ndim=1)

			out[keys[3]] 	=	ElemwiseSumLayer([out[keys[1]], out[keys[2]]])

			out[keys[-1]] 	=	NonlinearityLayer(out[keys[3]], nonlinearity=nonlinearity)
	else:
		# All the dimensions match and we can simply perform the addition operation
		keys = ['{}.{}.{}'.format(name,'conv2','nonlinearity'),
				'{}.{}'.format(name, 'elemwisesum'),
				'{}.{}'.format(name,'residual')]

		out[keys[1]] 		=	ElemwiseSumLayer([incoming, out[keys[0]]])

		out[keys[-1]]		=	NonlinearityLayer(out[keys[1]] , nonlinearity=nonlinearity)

	return out 

def build_vgg_plain_block(incoming, name, filter_size, increase_filters, W_init, nonlinearity=rectify):
	""" Implements a version of the Deep Plain Blocks with no skip layer connections. Equivalent to a deep stack of conv-layers. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* filter_size : int 
		An integer specifying the receptive field size of each of the filters.

	* increase_filters : boolean
		True/False indicating whether or not we are cutting the spatial size in half (N,N) -> (N/2, N/2) and doubling the number of convolutional filters. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let name = 'plainblock1_1', filter_size = 3 and increase_filters = True. This function will return an OrderedDict with the following keys:

		(1) 'plainblock1_1.conv1.conv'
		(2) 'plainblock1_1.conv1.bn'
		(3) 'plainblock1_1.conv1.nonlinearity'
		(4) 'plainblock1_1.conv2.conv'
		(5) 'plainblock1_1.conv2.bn'
		(6) 'plainblock1_1.conv2.nonlinearity'

	Reference
	---------
	Title:		Deep Residual Learning for Image Recognition

	Authors: 	Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun

	arXiv:		http://arxiv.org/abs/1512.03385

	"""
	out = OrderedDict()

	num_input_filters = incoming.output_shape[1]

	if increase_filters:
		first_stride = 2
		num_output_filters = 2*num_input_filters
	else:
		first_stride = 1
		num_output_filters = num_input_filters

	out.update(	build_conv_bn_layer(
					incoming=incoming, 
					name='{}.{}'.format(name,'conv1'), 
					num_filters=num_output_filters, 
					filter_size=filter_size, 
					stride=first_stride, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	out.update(	build_conv_bn_layer(
					incoming=out['{}.{}.{}'.format(name, 'conv1', 'nonlinearity')], 
					name='{}.{}'.format(name,'conv2'), 
					num_filters=num_output_filters, 
					filter_size=filter_size, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	return out 

def build_inception_residual_block(incoming, name, filter_sizes, increase_filters, projection, W_init, nonlinearity=rectify):
	""" Implements a version of the Deep Residual Blocks with Inception Layers. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* filter_sizes : list of ints 
		A list of integers specifying the receptive field size of each of the convolutional layers.

	* increase_filters : boolean
		True/False indicating whether or not we are cutting the spatial size in half (N,N) -> (N/2, N/2) and doubling the number of convolutional filters. 

		For the Inception Residual Blocks, we assume that each receptive field size has the same number of filters, thus we require that the number of input filters N is divisible by the number of different filter sizes.

	* projection: boolean 
		True/False indicating if we are using identity skip connections or projection skip connections. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let name = 'Inception_ResBlock1_1', num_layers = 2, filter_sizes=[3,5], increase_filters = True, projection = False so this is each Inception layer which will be the concatenation of a 3x3 conv layer and a 5x5 conv layer. This function will return an OrderedDict with the following keys:

		(1)  'Inception_Resblock1_1.inception1.conv3.conv'
		(2)  'Inception_Resblock1_1.inception1.conv3.bn'	
		(3)  'Inception_Resblock1_1.inception1.conv3.nonlinearity' 
		(4)  'Inception_Resblock1_1.inception1.conv5.conv'
		(5)  'Inception_Resblock1_1.inception1.conv5.bn'	
		(6)  'Inception_Resblock1_1.inception1.conv5.nonlinearity'
		(7)  'Inception_Resblock1_1.inception1.concat'
		(8)  'Inception_Resblock1_1.inception2.conv3.conv'
		(9)  'Inception_Resblock1_1.inception2.conv3.bn'	
		(10) 'Inception_Resblock1_1.inception2.conv3.nonlinearity' 
		(11) 'Inception_Resblock1_1.inception2.conv5.conv'
		(12) 'Inception_Resblock1_1.inception2.conv5.bn'	
		(13) 'Inception_Resblock1_1.inception2.conv5.nonlinearity'
		(14) 'Inception_Resblock1_1.inception2.concat'
		(15) 'Inception_Resblock1_1.subsample'
		(16) 'Inception_Resblock1_1.padded'
		(17) 'Inception_Resblock1_1.elemwisesum'
		(18) 'Inception_Resblock1_1.residual'

	If instead we had projection = True:

		(1)  'Inception_Resblock1_1.inception1.conv3.conv'
		(2)  'Inception_Resblock1_1.inception1.conv3.bn'	
		(3)  'Inception_Resblock1_1.inception1.conv3.nonlinearity' 
		(4)  'Inception_Resblock1_1.inception1.conv5.conv'
		(5)  'Inception_Resblock1_1.inception1.conv5.bn'	
		(6)  'Inception_Resblock1_1.inception1.conv5.nonlinearity'
		(7)  'Inception_Resblock1_1.inception1.concat'
		(8)  'Inception_Resblock1_1.inception2.conv3.conv'
		(9)  'Inception_Resblock1_1.inception2.conv3.bn'	
		(10) 'Inception_Resblock1_1.inception2.conv3.nonlinearity' 
		(11) 'Inception_Resblock1_1.inception2.conv5.conv'
		(12) 'Inception_Resblock1_1.inception2.conv5.bn'	
		(13) 'Inception_Resblock1_1.inception2.conv5.nonlinearity'
		(14) 'Inception_Resblock1_1.inception2.concat'
		(15) 'Inception_Resblock1_1.projection.conv'
		(16) 'Inception_Resblock1_1.projection.bn'
		(17) 'Inception_Resblock1_1.projection.nonlinearity'
		(18) 'Inception_Resblock1_1.elemwisesum'
		(19) 'Inception_Resblock1_1.residual'

	"""
	out = OrderedDict()

	num_inception_scales = len(filter_sizes)
	# Number of output filters must be divisible by the number of different filter sizes/scales:
	assert (incoming.output_shape[1] % num_inception_scales == 0), "The number of input filters must be divisible by the number of different Inception scales/receptive field sizes."
	num_input_filters = incoming.output_shape[1] / num_inception_scales

	if increase_filters:
		first_stride = 2
		list_of_num_output_filters = [2 * num_input_filters for i in xrange(num_inception_scales)]
	else:
		first_stride = 1
		list_of_num_output_filters = [num_input_filters for i in xrange(num_inception_scales)]

	first_strides = [first_stride for i in xrange(num_inception_scales)]
	strides = [1 for i in xrange(num_inception_scales)]
	pads = ['same' for i in xrange(num_inception_scales)]

	out.update(build_multi_scale_inception_bn_layer(
				incoming=incoming, 
				name='{}.{}'.format(name,'inception1'), 
				num_filters=list_of_num_output_filters, 
				filter_sizes=filter_sizes, 
				strides=first_strides, 
				pads=pads, 
				W_init=W_init, 
				nonlinearity=nonlinearity))

	out.update(build_multi_scale_inception_bn_layer(
				incoming=out['{}.{}.{}'.format(name,'inception1','concat')],
				name='{}.{}'.format(name,'inception2'), 
				num_filters=list_of_num_output_filters, 
				filter_sizes=filter_sizes, 
				strides=strides, 
				pads=pads, 
				W_init=W_init, 
				nonlinearity=nonlinearity))

	if increase_filters:
		if projection:
			# Total number of output filters:
			total_num_output_filters = sum(list_of_num_output_filters)

			# Perform projection via Option B in the MSR Deep ResNet article i.e. via convolution with spatial stride 2, 2x filter increase:
			out.update(	build_conv_bn_layer(
					incoming=incoming, 
					name='{}.{}'.format(name,'projection'), 
					num_filters=total_num_output_filters, 
					filter_size=1, 
					stride=first_stride, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity)) # Linear or rectify?

			keys = ['{}.{}.{}'.format(name,'projection','nonlinearity'), 
					'{}.{}.{}'.format(name,'inception2','concat'), 
					'{}.{}'.format(name, 'elemwisesum'),
					'{}.{}'.format(name,'residual')]

			out[keys[2]] 	=	ElemwiseSumLayer([out[keys[0]], out[keys[1]]])

			out[keys[-1]] 	=	NonlinearityLayer(out[keys[2]], nonlinearity=nonlinearity)

		else:
			# Perform projection via Option A in the MSR Deep ResNet article, i.e. via spatial subsampling and filter dimension zero padding

			keys = ['{}.{}'.format(name,'subsample'), 
					'{}.{}'.format(name,'padded'), 
					'{}.{}.{}'.format(name,'inception2','concat'), 
					'{}.{}'.format(name, 'elemwisesum'),
					'{}.{}'.format(name,'residual')]

			# Subsample the spatial dimensions to decrease the spatial dimensionality
			out[keys[0]] 	=	ExpressionLayer(	
									incoming=incoming,
									function=lambda X: X[:,:,::2,::2],
									output_shape=lambda s: (s[0], s[1], s[2]//2, s[3]//2))

			# We pad the inputs filter dimension with zeros:
			total_num_output_filters = sum(list_of_num_output_filters)
			out[keys[1]] 	= 	PadLayer(	
									incoming=out[keys[0]],
									width=[total_num_output_filters//4,0,0],
									batch_ndim=1)

			out[keys[3]] 	=	ElemwiseSumLayer([out[keys[1]], out[keys[2]]])

			out[keys[-1]] 	=	NonlinearityLayer(out[keys[3]], nonlinearity=nonlinearity)

	else:
		# All the dimensions match and we can simply perform the addition operation
		keys = ['{}.{}.{}'.format(name,'inception2','concat'),
				'{}.{}'.format(name,'residual')]

		out[keys[-1]]		=	NonlinearityLayer(ElemwiseSumLayer([incoming, out[keys[0]]]), nonlinearity=nonlinearity)

	return out 

def build_inception_plain_block(incoming, name, filter_sizes, increase_filters, W_init, nonlinearity=rectify):
	""" Implements a version of the Deep Plain Blocks with Inception Layers. 

	Parameters
	----------

	* incoming : a lasagne.layers.Layer class instance.
		The layer that is being fed into this layer.

	* name : string 
		A string specifying the name of the network layer/module, e.g. 'conv1'.

	* filter_sizes : list of ints 
		A list of integers specifying the receptive field size of each of the convolutional layers.

	* increase_filters : boolean
		True/False indicating whether or not we are cutting the spatial size in half (N,N) -> (N/2, N/2) and doubling the number of convolutional filters. 

		For the Inception Plain Blocks, we assume that each receptive field size has the same number of filters, thus we require that the number of input filters N is divisible by the number of different filter sizes.

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let name = 'Inception_PlainBlock1_1', filter_sizes = [3,5] and increase_filters = True. This function will return an OrderedDict with the following keys:

		(1)  'Inception_PlainBlock1_1.inception1.conv3.conv'
		(2)  'Inception_PlainBlock1_1.inception1.conv3.bn'
		(3)  'Inception_PlainBlock1_1.inception1.conv3.nonlinearity'
		(4)  'Inception_PlainBlock1_1.inception1.conv5.conv'
		(5)  'Inception_PlainBlock1_1.inception1.conv5.bn'
		(6)  'Inception_PlainBlock1_1.inception1.conv5.nonlinearity'
		(7)  'Inception_PlainBlock1_1.inception1.concat'
		(8)  'Inception_PlainBlock1_1.inception2.conv3.conv'
		(9)  'Inception_PlainBlock1_1.inception2.conv3.bn'
		(10) 'Inception_PlainBlock1_1.inception2.conv3.nonlinearity'
		(11) 'Inception_PlainBlock1_1.inception2.conv5.conv'
		(12) 'Inception_PlainBlock1_1.inception2.conv5.bn'
		(13) 'Inception_PlainBlock1_1.inception2.conv5.nonlinearity'
		(14) 'Inception_PlainBlock1_1.inception2.concat'
	"""
	out = OrderedDict()

	num_inception_scales = len(filter_sizes)
	# Number of output filters must be divisible by the number of different filter sizes/scales:
	assert (incoming.output_shape[1] % num_inception_scales == 0), "The number of input filters must be divisible by the number of different Inception scales/receptive field sizes."
	num_input_filters = incoming.output_shape[1] / num_inception_scales

	if increase_filters:
		first_stride = 2
		list_of_num_output_filters = [2 * num_input_filters for i in xrange(num_inception_scales)]
	else:
		first_stride = 1
		list_of_num_output_filters = [num_input_filters for i in xrange(num_inception_scales)]

	first_strides = [first_stride for i in xrange(num_inception_scales)]
	strides = [1 for i in xrange(num_inception_scales)]
	pads = ['same' for i in xrange(num_inception_scales)]

	out.update(	build_multi_scale_inception_bn_layer(
					incoming=incoming, 
					name='{}.{}'.format(name, 'inception1'), 
					num_filters=list_of_num_output_filters, 
					filter_sizes=filter_sizes, 
					strides=first_strides, 
					pads=pads, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	out.update(	build_multi_scale_inception_bn_layer(
					incoming=out['{}.{}.{}'.format(name, 'inception1','concat')],
					name='{}.{}'.format(name,'inception2'), 
					num_filters=list_of_num_output_filters, 
					filter_sizes=filter_sizes, 
					strides=strides, 
					pads=pads, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	return out

###############################################################################
################################# Networks ####################################
###############################################################################

#############################
##### CIFAR-10 networks #####
#############################

def build_deep_resnet(stack_depth, filter_size=3, input_var=None, input_shape=(3,32,32), num_classes=10, W_init=HeNormal(gain='relu'), projection=False, nonlinearity=rectify):
	""" Constructs a Deep ResNet. It is VGG-style. 

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the number of deep residual blocks to stack together. 

	* filter_size : int 
		An integer specifying the receptive field size of each of the convolutional layers. Default is set to 3.

	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 10 for CIFAR-10 classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* projection: boolean 
		True/False indicating if we are using identity skip connections or projection skip connections. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let stack_depth = 2, filter_size = 3, projection = False. This function will return an OrderedDict with the following keys:

		(1)  'input'
		(2)  'conv1.conv'
		(3)  'conv1.bn'
		(4)  'conv1.nonlinearity'
		(5)  'resblock1_1.conv1.conv'
		(6)  'resblock1_1.conv1.bn'
		(7)  'resblock1_1.conv1.nonlinearity'
		(8)  'resblock1_1.conv2.conv'
		(9)  'resblock1_1.conv2.bn'
		(10) 'resblock1_1.conv2.nonlinearity'
		(11) 'resblock1_1.elemwisesum'
		(12) 'resblock1_1.residual'
		(13) 'resblock1_2.conv1.conv'
		(14) 'resblock1_2.conv1.bn'
		(15) 'resblock1_2.conv1.nonlinearity'
		(16) 'resblock1_2.conv2.conv'
		(17) 'resblock1_2.conv2.bn'
		(18) 'resblock1_2.conv2.nonlinearity'
		(19) 'resblock1_2.elemwisesum'
		(20) 'resblock1_2.residual'
		(21) 'resblock2_1.conv1.conv'
		(22) 'resblock2_1.conv1.bn'
		(23) 'resblock2_1.conv1.nonlinearity'
		(24) 'resblock2_1.conv2.conv'
		(25) 'resblock2_1.conv2.bn'
		(26) 'resblock2_1.conv2.nonlinearity'
		(27) 'resblock2_1.subsample'
		(28) 'resblock2_1.padded'
		(29) 'resblock2_1.elemwisesum'
		(30) 'resblock2_1.residual'
		(31) 'resblock2_2.conv1.conv'
		(32) 'resblock2_2.conv1.bn'
		(33) 'resblock2_2.conv1.nonlinearity'
		(34) 'resblock2_2.conv2.conv'
		(35) 'resblock2_2.conv2.bn'
		(36) 'resblock2_2.conv2.nonlinearity'
		(37) 'resblock2_2.elemwisesum'
		(38) 'resblock2_2.residual'
		(39) 'resblock3_1.conv1.conv'
		(40) 'resblock3_1.conv1.bn'
		(41) 'resblock3_1.conv1.nonlinearity'
		(42) 'resblock3_1.conv2.conv'
		(43) 'resblock3_1.conv2.bn'
		(44) 'resblock3_1.conv2.nonlinearity'
		(45) 'resblock3_1.subsample'
		(46) 'resblock3_1.padded'
		(47) 'resblock3_1.elemwisesum'
		(48) 'resblock3_1.residual'
		(49) 'resblock3_2.conv1.conv'
		(50) 'resblock3_2.conv1.bn'
		(51) 'resblock3_2.conv1.nonlinearity'
		(52) 'resblock3_2.conv2.conv'
		(53) 'resblock3_2.conv2.bn'
		(54) 'resblock3_2.conv2.nonlinearity'
		(55) 'resblock3_2.elemwisesum'
		(56) 'resblock3_2.residual'
		(57) 'pool'
		(58) 'logits'
		(59) 'softmax'
	"""

	C, H, W = input_shape

	net = OrderedDict() 

	net['input']		=	InputLayer(	shape=(None,C,H,W),
										input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=16, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the first block of residual modules:
	net.update(build_vgg_residual_block(
					incoming=net['conv1.nonlinearity'], 
					name='resblock1_1', 
					filter_size=filter_size, 
					increase_filters=False, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_residual_block(
					incoming=net['resblock1_%d.residual' % (i)], 
					name='resblock1_%d' % (i+1), 
					filter_size=filter_size, 
					increase_filters=False, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the second block of residual modules:
	net.update(build_vgg_residual_block(
					incoming=net['resblock1_%d.residual' % (stack_depth)], 
					name='resblock2_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_residual_block(
					incoming=net['resblock2_%d.residual' % (i)], 
					name='resblock2_%d' % (i+1), 
					filter_size=filter_size, 
					increase_filters=False, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the third block of residual modules:
	net.update(build_vgg_residual_block(
					incoming=net['resblock2_%d.residual' % (stack_depth)], 
					name='resblock3_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_residual_block(
					incoming=net['resblock3_%d.residual' % (i)], 
					name='resblock3_%d' % (i+1), 
					filter_size=filter_size, 
					increase_filters=False, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Global Average Pooling Layer:
	net['pool']		=	GlobalPoolLayer(net['resblock3_%d.residual' % (stack_depth)])

	net['logits']	=	DenseLayer(	incoming=net['pool'],
									num_units=num_classes,
									W=HeNormal(),
									nonlinearity=None)

	net['softmax']	=	NonlinearityLayer(	incoming=net['logits'], 
											nonlinearity=softmax)

	return net

def build_deep_plainnet(stack_depth, filter_size=3, input_var=None, input_shape=(3,32,32), num_classes=10, W_init=HeNormal(gain='relu'), nonlinearity=rectify):
	""" Constructs a Deep PlainNet. It is VGG-style. 

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the number of deep plain blocks to stack together. 

	* filter_size : int 
		An integer specifying the receptive field size of each of the convolutional layers. Default is set to 3.

	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 10 for CIFAR-10 classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	Example
	-------

	Example output of this function: let stack_depth = 2, filter_size = 3. This function will return an OrderedDict with the following keys:

		(1)  'input'
		(2)  'conv1.conv'
		(3)  'conv1.bn'
		(4)  'conv1.nonlinearity'
		(5)  'plainblock1_1.conv1.conv'
		(6)  'plainblock1_1.conv1.bn'
		(7)  'plainblock1_1.conv1.nonlinearity'
		(8)  'plainblock1_1.conv2.conv'
		(9)  'plainblock1_1.conv2.bn'
		(10) 'plainblock1_1.conv2.nonlinearity'
		(11) 'plainblock1_2.conv1.conv'
		(12) 'plainblock1_2.conv1.bn'
		(13) 'plainblock1_2.conv1.nonlinearity'
		(14) 'plainblock1_2.conv2.conv'
		(15) 'plainblock1_2.conv2.bn'
		(16) 'plainblock1_2.conv2.nonlinearity'
		(17) 'plainblock2_1.conv1.conv'
		(18) 'plainblock2_1.conv1.bn'
		(19) 'plainblock2_1.conv1.nonlinearity'
		(20) 'plainblock2_1.conv2.conv'
		(21) 'plainblock2_1.conv2.bn'
		(22) 'plainblock2_1.conv2.nonlinearity'
		(23) 'plainblock2_2.conv1.conv'
		(24) 'plainblock2_2.conv1.bn'
		(25) 'plainblock2_2.conv1.nonlinearity'
		(26) 'plainblock2_2.conv2.conv'
		(27) 'plainblock2_2.conv2.bn'
		(28) 'plainblock2_2.conv2.nonlinearity'
		(29) 'plainblock3_1.conv1.conv'
		(30) 'plainblock3_1.conv1.bn'
		(31) 'plainblock3_1.conv1.nonlinearity'
		(32) 'plainblock3_1.conv2.conv'
		(33) 'plainblock3_1.conv2.bn'
		(34) 'plainblock3_1.conv2.nonlinearity'
		(35) 'plainblock3_2.conv1.conv'
		(36) 'plainblock3_2.conv1.bn'
		(37) 'plainblock3_2.conv1.nonlinearity'
		(38) 'plainblock3_2.conv2.conv'
		(39) 'plainblock3_2.conv2.bn'
		(40) 'plainblock3_2.conv2.nonlinearity'
		(41) 'pool'
		(42) 'logits'
		(43) 'softmax'
	"""

	C, H, W = input_shape

	net = OrderedDict() 

	net['input']		=	InputLayer(	shape=(None, C, H, W),
										input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=16, 
					filter_size=filter_size, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the first stack of plain blocks:
	net.update(build_vgg_plain_block(
					incoming=net['conv1.nonlinearity'], 
					name='plainblock1_1', 
					filter_size=filter_size, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	for i in range(1, stack_depth):
		net.update(build_vgg_plain_block(
					incoming=net['plainblock1_%d.conv2.nonlinearity' % (i)], 
					name='plainblock1_%d' % (i+1), 
					filter_size=filter_size, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the second stack of plain blocks:
	net.update(build_vgg_plain_block(
					incoming=net['plainblock1_%d.conv2.nonlinearity' % (stack_depth)], 
					name='plainblock2_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_plain_block(
					incoming=net['plainblock2_%d.conv2.nonlinearity' % (i)], 
					name='plainblock2_%d' % (i+1), 
					filter_size=filter_size, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the third stack of plain blocks:
	net.update(build_vgg_plain_block(
					incoming=net['plainblock2_%d.conv2.nonlinearity' % (stack_depth)], 
					name='plainblock3_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_plain_block(
					incoming=net['plainblock3_%d.conv2.nonlinearity' % (i)], 
					name='plainblock3_%d' % (i+1), 
					filter_size=filter_size, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Global Average Pooling Layer:
	net['pool']		=	GlobalPoolLayer(net['plainblock3_%d.conv2.nonlinearity' % (stack_depth)])

	net['logits']	=	DenseLayer(	incoming=net['pool'],
									num_units=num_classes,
									W=HeNormal(),
									nonlinearity=None)

	net['softmax']	=	NonlinearityLayer(	incoming=net['logits'], 
											nonlinearity=softmax)

	return net

def build_nagadomi_bn(input_var=None, input_shape=(3,32,32), num_classes=10, W_init=HeNormal(gain='relu'), nonlinearity=rectify):
	""" Builds a Nagadomi (VGG style) network with Batch Normalization. 

	Parameters
	----------
	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 10 for CIFAR-10 classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.	

	Reference
	---------
	Nagadomi Network: https://github.com/nagadomi/kaggle-cifar10-torch7.

	"""

	C, H, W = input_shape 

	net = OrderedDict()

	net['input'] 	=	InputLayer(	shape=(None, C, H, W),
									input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=64, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net.update(build_conv_bn_layer(
					incoming=net['conv1.nonlinearity'], 
					name='conv2', 
					num_filters=64, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net['pool1']	=	MaxPool2DLayer(
							incoming=net['conv2.nonlinearity'],
							pool_size=(2,2))

	net.update(build_conv_bn_layer(
					incoming=dropout(net['pool1'], p=0.25),
					name='conv3', 
					num_filters=128, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net.update(build_conv_bn_layer(
					incoming=net['conv3.nonlinearity'], 
					name='conv4', 
					num_filters=128, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net['pool2']	=	MaxPool2DLayer(
							incoming=net['conv4.nonlinearity'],
							pool_size=(2,2))

	net.update(build_conv_bn_layer(
					incoming=dropout(net['pool2'], p=0.25),
					name='conv5', 
					num_filters=256, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net.update(build_conv_bn_layer(
					incoming=net['conv5.nonlinearity'], 
					name='conv6', 
					num_filters=256, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net.update(build_conv_bn_layer(
					incoming=net['conv6.nonlinearity'],
					name='conv7', 
					num_filters=256, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net.update(build_conv_bn_layer(
					incoming=net['conv7.nonlinearity'], 
					name='conv8', 
					num_filters=256, 
					filter_size=3, 
					stride=1, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	net['pool3']	=	MaxPool2DLayer(
							incoming=net['conv8.nonlinearity'],
							pool_size=(2,2))

	net.update(build_fc_bn_module(
					incoming=dropout(net['pool3'], p=0.25), 
					name='fc1', 
					num_units=1024, 
					W_init=W_init, 
					nonlinearity=rectify))

	net.update(build_fc_bn_module(
					incoming=dropout(net['fc1.nonlinearity'], p=0.25), 
					name='fc2.logits', 
					num_units=1024, 
					W_init=W_init, 
					nonlinearity=linear))

	net['softmax']	=	DenseLayer(	
						incoming=dropout(net['fc2.logits.nonlinearity'], p=.5),
						num_units=num_classes,
						W=HeNormal(),
						nonlinearity=softmax)

	return net

##############################
##### Pentomino Networks #####
##############################

def build_pentomino_deep_vgg_resnet(stack_depth, filter_size=3, first_stride=2, input_var=None, input_shape=(1,64,64), num_classes=2, W_init=HeNormal(gain='relu'), projection=False, nonlinearity=rectify):
	""" Constructs a Deep ResNet with an initial conv layer with a large receptive field size and a large stride.

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the number of deep residual blocks to stack together. 

	* filter_size : int 
		An integer specifying the receptive field size of each of the convolutional layers. Default is set to 3.

	* first_stride : int 
		An integer specifying the length of the frist convolutional stride. Default is set to 2. 

	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 2 for Pentomino classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* projection: boolean 
		True/False indicating if we are using identity skip connections or projection skip connections. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	"""

	C, H, W = input_shape

	net = OrderedDict() 

	net['input']		=	InputLayer(	shape=(None,C,H,W),
										input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=16, 
					filter_size=7, 
					stride=first_stride,
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the first block of residual modules:
	net.update(build_vgg_residual_block(
					incoming=net['conv1.nonlinearity'], 
					name='resblock1_1', 
					filter_size=filter_size, 
					increase_filters=False, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_residual_block(
						incoming=net['resblock1_%d.residual' % (i)], 
						name='resblock1_%d' % (i+1), 
						filter_size=filter_size, 
						increase_filters=False, 
						projection=projection, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Add the second block of residual modules:
	net.update(build_vgg_residual_block(
					incoming=net['resblock1_%d.residual' % (stack_depth)],
					name='resblock2_1', 
					filter_size=filter_size, 
					increase_filters=True,
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_residual_block(
						incoming=net['resblock2_%d.residual' % (i)], 
						name='resblock2_%d' % (i+1), 
						filter_size=filter_size, 
						increase_filters=False, 
						projection=projection, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Add the third block of residual modules:
	net.update(build_vgg_residual_block(
					incoming=net['resblock2_%d.residual' % (stack_depth)], 
					name='resblock3_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_residual_block(
						incoming=net['resblock3_%d.residual' % (i)], 
						name='resblock3_%d' % (i+1), 
						filter_size=filter_size, 
						increase_filters=False, 
						projection=projection, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Global Average Pooling Layer:
	net['pool']		=	GlobalPoolLayer(net['resblock3_%d.residual' % (stack_depth)])

	net['logits']	=	DenseLayer(	incoming=net['pool'],
									num_units=num_classes,
									W=HeNormal(),
									nonlinearity=None)

	net['softmax']	=	NonlinearityLayer(	incoming=net['logits'], 
											nonlinearity=softmax)

	return net

def build_pentomino_deep_vgg_plainnet(stack_depth, filter_size=3, first_stride=2, input_var=None, input_shape=(1,64,64), num_classes=2, W_init=HeNormal(gain='relu'), nonlinearity=rectify):
	""" Constructs a Deep PlainNet with an initial conv layer with a large receptive field size and a large stride.

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the number of deep plain blocks to stack together. 

	* filter_size : int 
		An integer specifying the receptive field size of each of the convolutional layers. Default is set to 3.

	* first_stride : int 
		An integer specifying the length of the frist convolutional stride. Default is set to 2. 

	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 2 for Pentomino classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	"""

	C, H, W = input_shape

	net = OrderedDict() 

	net['input']		=	InputLayer(	shape=(None,C,H,W),
										input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=16, 
					filter_size=7, 
					stride=first_stride, 
					pad='same', 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	# Add the first stack of plain blocks:
	net.update(build_vgg_plain_block(
					incoming=net['conv1.nonlinearity'], 
					name='plainblock1_1', 
					filter_size=filter_size, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	for i in range(1, stack_depth):
		net.update(build_vgg_plain_block(
						incoming=net['plainblock1_%d.conv2.nonlinearity' % (i)], 
						name='plainblock1_%d' % (i+1), 
						filter_size=filter_size, 
						increase_filters=False, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Add the second stack of plain blocks:
	net.update(build_vgg_plain_block(
					incoming=net['plainblock1_%d.conv2.nonlinearity' % (stack_depth)], 
					name='plainblock2_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_plain_block(
						incoming=net['plainblock2_%d.conv2.nonlinearity' % (i)], 
						name='plainblock2_%d' % (i+1), 
						filter_size=filter_size, 
						increase_filters=False, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Add the third stack of plain blocks:
	net.update(build_vgg_plain_block(
					incoming=net['plainblock2_%d.conv2.nonlinearity' % (stack_depth)],
					name='plainblock3_1', 
					filter_size=filter_size, 
					increase_filters=True, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_vgg_plain_block(
						incoming=net['plainblock3_%d.conv2.nonlinearity' % (i)],
						name='plainblock3_%d' % (i+1), 
						filter_size=filter_size, 
						increase_filters=False, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Global Average Pooling Layer:
	net['pool']		=	GlobalPoolLayer(net['plainblock3_%d.conv2.nonlinearity' % (stack_depth)])

	net['logits']	=	DenseLayer(	incoming=net['pool'],
									num_units=num_classes,
									W=HeNormal(),
									nonlinearity=None)

	net['softmax']	=	NonlinearityLayer(	incoming=net['logits'], 
											nonlinearity=softmax)

	return net

def build_pentomino_deep_inception_resnet(stack_depth, filter_sizes=[3,5], first_stride=2, input_var=None, input_shape=(1,64,64), num_classes=2, W_init=HeNormal(gain='relu'), projection=False, nonlinearity=rectify):
	""" Constructs a Deep Inception ResNet with an initial conv layer with a large receptive field size and a large stride.

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the number of deep residual blocks to stack together. 

	* num_layers_per_module: int
		An integer specifying the number of layers in each deep residual block. Default is set to 2.

	* filter_sizes : list of ints 
		A list of integers specifying the receptive field size of each of the convolutional layers. Default is set to [3,5].

	* first_stride : int 
		An integer specifying the length of the frist convolutional stride. Default is set to 2. 

	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 2 for Pentomino classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* projection: boolean 
		True/False indicating if we are using identity skip connections or projection skip connections. 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	"""

	C, H, W = input_shape

	net = OrderedDict() 

	net['input']		=	InputLayer(	shape=(None,C,H,W),
										input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=16, 
					filter_size=7, 
					stride=first_stride, 
					pad='same', 
					W_init=HeNormal(), 
					nonlinearity=nonlinearity))

	# Add the first block of residual modules:
	net.update(build_inception_residual_block(
					incoming=net['conv1.nonlinearity'], 
					name='resblock1_1', 
					filter_sizes=filter_sizes, 
					increase_filters=False, 
					projection=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	for i in range(1, stack_depth):
		net.update(build_inception_residual_block(
					incoming=net['resblock1_%d.residual' % (i)], 
					name='resblock1_%d' % (i+1), 
					filter_sizes=filter_sizes, 
					increase_filters=False, 
					projection=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	# Add the second block of residual modules:
	net.update(build_inception_residual_block(
					incoming=net['resblock1_%d.residual' % (stack_depth)],
					name='resblock2_1', 
					filter_sizes=filter_sizes, 
					increase_filters=True, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_inception_residual_block(
						incoming=net['resblock2_%d.residual' % (i)], 
						name='resblock2_%d' % (i+1), 
						filter_sizes=filter_sizes, 
						increase_filters=False, 
						projection=projection, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Add the third block of residual modules:
	net.update(build_inception_residual_block(
					incoming=net['resblock2_%d.residual' % (stack_depth)],
					name='resblock3_1', 
					filter_sizes=filter_sizes, 
					increase_filters=True, 
					projection=projection, 
					W_init=W_init, 
					nonlinearity=nonlinearity))

	for i in range(1, stack_depth):
		net.update(build_inception_residual_block(
						incoming=net['resblock3_%d.residual' % (i)], 
						name='resblock3_%d' % (i+1), 
						filter_sizes=filter_sizes, 
						increase_filters=False, 
						projection=projection, 
						W_init=W_init, 
						nonlinearity=nonlinearity))

	# Global Average Pooling Layer:
	net['pool']		=	GlobalPoolLayer(net['resblock3_%d.residual' % (stack_depth)])

	net['logits']	=	DenseLayer(	incoming=net['pool'],
									num_units=num_classes,
									W=HeNormal(),
									nonlinearity=None)

	net['softmax']	=	NonlinearityLayer(	incoming=net['logits'], 
											nonlinearity=softmax)

	return net

def build_pentomino_deep_inception_plainnet(stack_depth, filter_sizes=[3,5], first_stride=2, input_var=None, input_shape=(1,64,64), num_classes=2, W_init=HeNormal(gain='relu'), nonlinearity=rectify):
	""" Constructs a Deep Inception PlainNet with an initial conv layer with a large receptive field size and a large stride.

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the number of deep residual blocks to stack together. 

	* filter_sizes : list of ints 
		A list of integers specifying the receptive field size of each of the convolutional layers. Default is set to [3,5].

	* first_stride : int 
		An integer specifying the length of the frist convolutional stride. Default is set to 2. 

	* input_var : Theano symbolic variable 
		A Theano symbolic variable representing the input to the network.

	* input_shape : tuple of int
		A 3-tuple of integers (C,H,W) specifying the number of input channels C, and spatial height H and spatial width W. 

	* num_classes : int
		Integer specifying the number of output classes. Default to 2 for Pentomino classification. 

	* W_init : Theano shared variable, expression, numpy array or callable 
		Initial value for the weights of the convolutional filters. Default to HeNormal(gain='relu'). 

	* nonlinearity : callable or None 
		Specifies the nonlinearity applied to the output of the convolution operation. Default is set to 'rectify'.

	"""

	C, H, W = input_shape

	net = OrderedDict() 

	net['input']		=	InputLayer(	shape=(None,C,H,W),
										input_var=input_var)

	net.update(build_conv_bn_layer(
					incoming=net['input'], 
					name='conv1', 
					num_filters=16, 
					filter_size=7, 
					stride=first_stride, 
					pad='same', 
					W_init=HeNormal(), 
					nonlinearity=nonlinearity))

	# Add the first block of residual modules:
	net.update(build_inception_plain_block(
					incoming=net['conv1.nonlinearity'], 
					name='plainblock1_1', 
					filter_sizes=filter_sizes, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	for i in range(1, stack_depth):
		net.update(build_inception_plain_block(
					incoming=net['plainblock1_%d.inception2.concat' % (i)], 
					name='plainblock1_%d' % (i+1), 
					filter_sizes=filter_sizes, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	# Add the second block of residual modules:
	net.update(build_inception_plain_block(
					incoming=net['plainblock1_%d.inception2.concat' % (stack_depth)], 
					name='plainblock2_1', 
					filter_sizes=filter_sizes, 
					increase_filters=True, 
					W_init=W_init, 
					nonlinearity=rectify))

	for i in range(1, stack_depth):
		net.update(build_inception_plain_block(
					incoming=net['plainblock2_%d.inception2.concat' % (i)], 
					name='plainblock2_%d' % (i+1), 
					filter_sizes=filter_sizes, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	# Add the third block of residual modules:
	net.update(build_inception_plain_block(
					incoming=net['plainblock2_%d.inception2.concat' % (stack_depth)],
					name='plainblock3_1', 
					filter_sizes=filter_sizes, 
					increase_filters=True, 
					W_init=W_init, 
					nonlinearity=rectify))

	for i in range(1, stack_depth):
		net.update(build_inception_plain_block(
					incoming=net['plainblock3_%d.inception2.concat' % (i)],
					name='plainblock3_%d' % (i+1), 
					filter_sizes=filter_sizes, 
					increase_filters=False, 
					W_init=W_init, 
					nonlinearity=rectify))

	# Global Average Pooling Layer:
	net['pool']		=	GlobalPoolLayer(net['plainblock3_%d.inception2.concat' % (stack_depth)])

	net['logits']	=	DenseLayer(	incoming=net['pool'],
									num_units=num_classes,
									W=HeNormal(),
									nonlinearity=None)

	net['softmax']	=	NonlinearityLayer(	incoming=net['logits'], 
											nonlinearity=softmax)

	return net

if __name__ == '__main__':
	pass 