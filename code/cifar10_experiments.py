import theano, lasagne, os, time, glob
import numpy as np 
import theano.tensor as T
import cPickle as pickle
import matplotlib.pyplot as plt 
from collections import OrderedDict
from tsne import bh_sne
from lasagne.layers import get_output, get_all_layers, get_all_params
from lasagne.layers import set_all_param_values, get_all_param_values
from lasagne.layers import count_params
from lasagne.nonlinearities import softmax
from modules import momentum_snapshot_trainer, compute_activation_statistics
from modules import train_mimic_net
from utils import gcn, flips, iterate_minibatches
from data_load import cifar10_load
from networks import build_deep_resnet, build_deep_plainnet, build_nagadomi_bn

def train_n_resnet_and_plainnet(data_dir, stack_depth, num_trials, root_dir, save_iter=100, num_epochs=2, train_from_scratch=True):
	""" Will train multiple ResNet-N and PlainNet-N networks on the CIFAR-10 dataset using the momentum snapshot trainer function.

	Parameters
	----------
	* data_dir : string 
		A string specifying the file directory to CIFAR-10 dataset.

	* stack_depth : int 
		An integer specifying the depth of the Plain/Residual blocks in our networks.

	* num_trials : int
		An integer specifying the number of trials to perform.

	* root_dir : string
		A string specifying the root directory. Will have the following hierarchy:
			- graphics/
			- resnet/
				- 1/
				- .
				- .
				- num_trials/
			- plainnet/
				- 1/
				- .
				- .
				- num_trials/

	* save_iter : int 
		An integer specifying how many training batch updates to perform before we save the weights and evaluate on the test set. Default is set to 100.

	* num_epochs : int 
		An integer specifying the number of training epochs. Default is set to 52. 

	* train_from_scratch : boolean True/False
		A boolean True/False specifying whether or not to train the networks from scratch or whether or to load a previous training snapshot. Default is set to True.

	"""
	# Create the directory structure:
	plainnet_param_dir = root_dir+'plainnet/'
	resnet_param_dir = root_dir+'resnet/'

	if os.path.exists(root_dir) == False:
		os.makedirs(root_dir)
	if os.path.exists(root_dir+'graphics/') == False:
		os.makedirs(root_dir+'graphics/')
	if os.path.exists(resnet_param_dir) == False:
		os.makedirs(resnet_param_dir)

		for i in xrange(num_trials):
			if os.path.exists(resnet_param_dir+'%d/' % (i+1)) == False:
				os.makedirs(resnet_param_dir+'%d/' % (i+1))

	if os.path.exists(plainnet_param_dir) == False:
		os.makedirs(plainnet_param_dir)
		for i in xrange(num_trials):
			if os.path.exists(plainnet_param_dir+'%d/' % (i+1)) == False:
				os.makedirs(plainnet_param_dir+'%d/' % (i+1))

	# Load the CIFAR-10 data and process it: 
	print "Loading the CIFAR-10 dataset and processing it..."
	X_train, Y_train, X_val, Y_val, X_test, Y_test = cifar10_load(dir_path=data_dir, num_val=0) 
	X_train, X_val, X_test, mean_img, std_img = gcn(X_train, X_val, X_test, True)

	X_train_flipped = flips(X_train, 'horizontal')
	X_train = np.concatenate((X_train, X_train_flipped), axis=0)
	Y_train = np.concatenate((Y_train, Y_train), axis=0).astype('int32')
	Y_test = Y_test.astype('int32')

	data_dict = OrderedDict()
	data_dict['X_train'] = X_train 
	data_dict['Y_train'] = Y_train
	data_dict['X_test'] = X_test 
	data_dict['Y_test'] = Y_test 

	input_var = T.tensor4('inputs')
	target_var = T.ivector('targets')

	for i in xrange(num_trials):
		print "Currently training the %d of %d ResNet model..." % (i+1, num_trials)

		# Train a Deep ResNet: 
		resnet = build_deep_resnet(stack_depth=stack_depth, input_var=input_var)
		# Cache the initialized weights:
		init_weights = get_all_param_values(resnet['softmax'])

		# Define the save directory:
		resnet_dir = resnet_param_dir+'%d/' % (i+1)

		# Train it!
		if train_from_scratch:
			if os.path.isfile(resnet_dir+'train_snapshot.pkl') == True:
				print "Passed on Trial %d" % (i+1)
			else:
				train_snapshot = momentum_snapshot_trainer(
									net=resnet, 
									input_var=input_var, 
									target_var=target_var, 
									data_dict=data_dict, 
									save_iter=save_iter, 
									save_dir=resnet_dir, 
									num_epochs=num_epochs, 
									batchsize=128, 
									train_continue=False, 
									train_snapshot=None)
		else:
			# Load a snapshot:
			train_snapshot = pickle.load(open(resnet_dir+'train_snapshot.pkl', 'rb'))

			train_snapshot = momentum_snapshot_trainer(
								net=resnet, 
								input_var=input_var, 
								target_var=target_var, 
								data_dict=data_dict, 
								save_iter=save_iter, 
								save_dir=resnet_dir, 
								num_epochs=num_epochs, 
								batchsize=128, 
								train_continue=True, 
								train_snapshot=train_snapshot)

		print "Currently training the %d of %d PlainNet model..." % (i+1, num_trials)

		# Train a Deep PlainNet:
		plainnet = build_deep_plainnet(stack_depth=stack_depth, input_var=input_var)

		# Initialize the same weights as the ResNet:
		set_all_param_values(plainnet['softmax'], init_weights)

		# Define the save directory:
		plainnet_dir = plainnet_param_dir+'%d/' % (i+1)

		# Train it!
		if train_from_scratch:
			if os.path.isfile(plainnet_dir+'train_snapshot.pkl') == True:
				print "Passed on Trial %d" % (i+1)
			else:
				train_snapshot = momentum_snapshot_trainer(
									net=plainnet, 
									input_var=input_var, 
									target_var=target_var, 
									data_dict=data_dict, 
									save_iter=save_iter, 
									save_dir=plainnet_dir, 
									num_epochs=num_epochs, 
									batchsize=128, 
									train_continue=False, 
									train_snapshot=None)
		else:
			# Load a snapshot:
			train_snapshot = pickle.load(open(plainnet_dir+'train_snapshot.pkl', 'rb'))
			train_snapshot = momentum_snapshot_trainer(
								net=plainnet, 
								input_var=input_var, 
								target_var=target_var, 
								data_dict=data_dict, 
								save_iter=save_iter, 
								save_dir=plainnet_dir, 
								num_epochs=num_epochs, 
								batchsize=128, 
								train_continue=True, 
								train_snapshot=train_snapshot)

def activation_statistics_cifar10_experiment_n_trials(data_dir, stack_depth, num_trials, root_dir, compute_from_scratch=False, batchsize=500):
	""" Given trained ResNet-N and PlainNet-N models, this function computes the average activation statistics for a ResNet and a PlainNet between varying training intervals. 

	Parameters
	----------

	* data_dir : string 
		A string specifying the file directory to CIFAR-10 dataset.

	* stack_depth : int 
		An integer specifying the depth of the Plain/Residual blocks in our networks.

	* num_trials : int
		An integer specifying the number of different sets of weights.

	* batchsize : int 
		An integer specifying the size of each training batch. Default is set to 500.

	* root_dir : string
		A string specifying the root directory. Must have the following hierarchy:
			- graphics/
			- resnet/
				- 1/
				- .
				- .
				- num_trials/
			- plainnet/
				- 1/
				- .
				- .
				- num_trials/

	* compute_from_scratch: boolean True/False
		A boolean True/False indicating whether or not to compute the activation statistics from scratch. If set to False, a stats_dict.pkl file must be present in the save_dir. 

	"""
	# Define directory strings for convenience:
	resnet_param_dir = root_dir+'resnet/'
	plainnet_param_dir = root_dir+'plainnet/'
	graphics_dir = root_dir+'graphics/'

	# Check that the root_dir has the correct the hierarchical structure:
	assert os.path.exists(graphics_dir), "Graphics directory does not exist."
	for i in xrange(num_trials):
		assert os.path.exists(plainnet_param_dir+'%d/' % (i+1)), "Subfolder %d is missing from plainnet_param_dir." % (i+1)
		assert os.path.exists(resnet_param_dir+'%d/' % (i+1)), "Subfolder %d is missing from resnet_param_dir." % (i+1)

	# Load the CIFAR-10 data and process it: 
	print "Loading the CIFAR-10 dataset and processing it..."
	X_train, Y_train, X_val, Y_val, X_test, Y_test = cifar10_load(dir_path=data_dir, num_val=0) 
	X_train, X_val, X_test, mean_img, std_img = gcn(X_train, X_val, X_test, True)
	X_train = np.concatenate((X_train, flips(X_train, 'horizontal')))
	Y_train = np.concatenate((Y_train, Y_train)) 

	# Initialize Theano symbolic variables:
	input_var = T.tensor4('inputs')

	# Initialize a ResNet and a PlainNet:
	resnet = build_deep_resnet(stack_depth=stack_depth, input_var=input_var)
	plainnet = build_deep_plainnet(stack_depth=stack_depth, input_var=input_var)

	# Define the training time intervals:
	time_intervals = [(1000, 5000),
					  (5000, 10000),
					  (10000, 20000),
					  (20000, 30000),
					  (30000, 35000),
					  (35000, 40000)]

	# Define the keys for the activation statistics to compute:
	stat_keys = ['total_units',
				 'deactivate_agree',
				 'deactivate_new',
				 'activate_new',
				 'activate_agree']

	########################################################################
	##### For each stack_depth define the appropriate layer keys, etc. #####
	########################################################################

	if stack_depth == 3 or stack_depth == 5:
		# Define the residual layer keys:
		res_layer_keys = ['conv1.nonlinearity',
						  'resblock1_1.residual',
						  'resblock1_%d.residual' % stack_depth,
						  'resblock2_1.residual',
						  'resblock2_%d.residual' % stack_depth,
						  'resblock3_1.residual',
						  'resblock3_%d.residual' % stack_depth]

		# Define the plain layer keys:
		plain_layer_keys = ['conv1.nonlinearity',
							'plainblock1_1.conv2.nonlinearity',
							'plainblock1_%d.conv2.nonlinearity' % stack_depth,
							'plainblock2_1.conv2.nonlinearity',
							'plainblock2_%d.conv2.nonlinearity' % stack_depth,
							'plainblock3_1.conv2.nonlinearity',
							'plainblock3_%d.conv2.nonlinearity' % stack_depth]

		title_keys = ['CONV1',
					  'DeepBlock1-1',
					  'DeepBlock1-%d' % stack_depth,
					  'DeepBlock2-1',
					  'DeepBlock2-%d' % stack_depth,
					  'DeepBlock3-1',
					  'DeepBlock3-%d' % stack_depth]

		# Zip the layer keys together: 
		layer_keys = zip(res_layer_keys, plain_layer_keys)
	elif stack_depth == 7:
		# Define the residual layer keys:
		res_layer_keys = ['conv1.nonlinearity',
						  'resblock1_1.residual',
						  'resblock1_4.residual',
						  'resblock1_%d.residual' % stack_depth,
						  'resblock2_1.residual',
						  'resblock2_4.residual',
						  'resblock2_%d.residual' % stack_depth,
						  'resblock3_1.residual',
						  'resblock3_4.residual',
						  'resblock3_%d.residual' % stack_depth]

		# Define the plain layer keys:
		plain_layer_keys = ['conv1.nonlinearity',
							'plainblock1_1.conv2.nonlinearity',
							'plainblock1_4.conv2.nonlinearity',
							'plainblock1_%d.conv2.nonlinearity' % stack_depth,
							'plainblock2_1.conv2.nonlinearity',
							'plainblock2_4.conv2.nonlinearity',
							'plainblock2_%d.conv2.nonlinearity' % stack_depth,
							'plainblock3_1.conv2.nonlinearity',
							'plainblock3_4.conv2.nonlinearity',
							'plainblock3_%d.conv2.nonlinearity' % stack_depth]

		title_keys = ['CONV1',
					  'DeepBlock1-1',
					  'DeepBlock1-4',
					  'DeepBlock1-%d' % stack_depth,
					  'DeepBlock2-1',
					  'DeepBlock2-4',
					  'DeepBlock2-%d' % stack_depth,
					  'DeepBlock3-1',
					  'DeepBlock3-4',
					  'DeepBlock3-%d' % stack_depth]

		# Zip the layer keys together: 
		layer_keys = zip(res_layer_keys, plain_layer_keys)

	# Create a dictionary to store all the activation statistics:
	res_stats_dict = OrderedDict.fromkeys(res_layer_keys)
	for key in res_stats_dict:
		res_stats_dict[key] = OrderedDict.fromkeys(time_intervals)
		for interval in time_intervals:
			res_stats_dict[key][interval] = OrderedDict().fromkeys(stat_keys)
			for stat_key in stat_keys:
				res_stats_dict[key][interval][stat_key] = np.zeros(num_trials)

	plain_stats_dict = OrderedDict.fromkeys(plain_layer_keys)
	for key in plain_stats_dict:
		plain_stats_dict[key] = OrderedDict.fromkeys(time_intervals)
		for interval in time_intervals:
			plain_stats_dict[key][interval] = OrderedDict().fromkeys(stat_keys)
			for stat_key in stat_keys:
				plain_stats_dict[key][interval][stat_key] = np.zeros(num_trials)
	if compute_from_scratch:
		for i in xrange(num_trials):
			print "Computing statistics for the %d-th set of weights..." % (i+1)
			for index, value in enumerate(layer_keys):
				res_layer_key, plain_layer_key = value

				print "Compiling ResNet and PlainNet forward pass functions..."
				res_out = get_output(resnet[res_layer_key])
				get_res_out = theano.function([input_var], res_out)
				plain_out = get_output(plainnet[plain_layer_key])
				get_plain_out = theano.function([input_var], plain_out)

				for interval in time_intervals:
					init_batch, final_batch = interval 
					print "Computing activation statistics for %d layer between init %d and final %d..." % (index, init_batch, final_batch)

					for inputs, targets in iterate_minibatches(X_train, Y_train, batchsize=batchsize, shuffle=False):

						# Compute ResNet init_params output:
						with np.load(resnet_param_dir+'%d/%d_iter_completed_weights.npz' % (i+1, init_batch)) as f:
							resnet_init_params = [f['arr_%d' % j] for j in range(len(f.files))]
						set_all_param_values(resnet['softmax'], resnet_init_params)
						res_init_output = get_res_out(inputs)

						# Compute ResNet final_params output:
						with np.load(resnet_param_dir+'%d/%d_iter_completed_weights.npz' % (i+1, final_batch)) as f:
							resnet_final_params = [f['arr_%d' % j] for j in range(len(f.files))]
						set_all_param_values(resnet['softmax'], resnet_final_params)
						res_final_output = get_res_out(inputs)

						res_batch_dict = compute_activation_statistics(
											x_init=res_init_output, 
											x_final=res_final_output,
											delta=0)

						# Update the res_stats_dict with the res_batch_dict stats:
						for key in res_stats_dict[res_layer_key][interval]:
							res_stats_dict[res_layer_key][interval][key][i] += res_batch_dict[key]

						# Compute PlainNet init_params output:
						with np.load(plainnet_param_dir+'%d/%d_iter_completed_weights.npz' % (i+1,init_batch)) as f:
							plainnet_init_params = [f['arr_%d' % j] for j in range(len(f.files))]
						set_all_param_values(plainnet['softmax'], plainnet_init_params)
						plain_init_output = get_plain_out(inputs)

						# Compute PlainNet final_params output:
						with np.load(plainnet_param_dir+'%d/%d_iter_completed_weights.npz' % (i+1, final_batch)) as f:
							plainnet_final_params = [f['arr_%d' % j] for j in range(len(f.files))]
						set_all_param_values(plainnet['softmax'], plainnet_final_params)
						plain_final_output = get_plain_out(inputs)

						plain_batch_dict = compute_activation_statistics(
											x_init=plain_init_output, 
											x_final=plain_final_output,
											delta=0)

						# Update the plain_stats_dict with the plain_batch_dict stats:
						for key in plain_stats_dict[plain_layer_key][interval]:
							plain_stats_dict[plain_layer_key][interval][key][i] += plain_batch_dict[key]

					# Normalize all of the activation statistics:
					for key in res_stats_dict[res_layer_key][interval]:
						if key != 'total_units':
							res_stats_dict[res_layer_key][interval][key][i] /= float(res_stats_dict[res_layer_key][interval]['total_units'][i])
					for key in plain_stats_dict[plain_layer_key][interval]:
						if key != 'total_units':
							plain_stats_dict[plain_layer_key][interval][key][i] /= float(plain_stats_dict[plain_layer_key][interval]['total_units'][i])

		# Save all the computed statistics:
		stats = OrderedDict()
		stats['res'] = res_stats_dict
		stats['plain'] = plain_stats_dict
		pickle.dump(stats, open(root_dir+'stats_dict.pkl', 'wb'))
		
	else: 
		# Load the computed statistics. This is primarily for debugging purposes and to shape the graphics properly:
		stats = pickle.load(open(root_dir+'stats_dict.pkl', 'rb'))
		res_stats_dict = stats['res']
		plain_stats_dict = stats['plain']

	# Create 4x2 percentage barplots for each layer key for each time interval:
	for index, value in enumerate(layer_keys):
		res_layer_key, plain_layer_key = value
		title_key = title_keys[index]
		for interval in time_intervals:
			init_batch, final_batch = interval 
			# Compute the means and errs for each statistic:
			res_means = np.array([res_stats_dict[res_layer_key][interval].items()[1:][i][1].mean() for i in xrange(4)])
			res_std = np.array([res_stats_dict[res_layer_key][interval].items()[1:][i][1].std() for i in xrange(4)])

			plain_means = np.array([plain_stats_dict[plain_layer_key][interval].items()[1:][i][1].mean() for i in xrange(4)])
			plain_std = np.array([plain_stats_dict[plain_layer_key][interval].items()[1:][i][1].std() for i in xrange(4)])

			N = 4 
			ind = np.arange(N)
			width = 0.35 
			fig, ax = plt.subplots()
			rects1 = ax.bar(ind, 100*res_means, width, color='r', yerr=100*res_std)
			rects2 = ax.bar(ind+width, 100*plain_means, width, color='b', yerr=100*plain_std)
			ax.set_ylabel('Percentage %')
			if index == 0:
				ax.set_title('%s Activations: %d to %d' % (title_key, init_batch, final_batch))
			else:
				ax.set_title('%s Activations: %d to %d' %  (title_key, init_batch, final_batch))
			ax.set_xticks(ind + width)
			ax.set_xticklabels(('Deactivate-Agree', 'Deactivate-New', 'Activate-New', 'Activate-Agree'))
			ax.legend((rects1[0], rects2[0]), ('Res', 'Plain'), loc='upper center')
			plt.tight_layout()
			if index == 0:
				plt.savefig(graphics_dir+'%s_act_stats_percentage_%d_%d.png' % (title_key, init_batch, final_batch))
			else:
				plt.savefig(graphics_dir+'%s_act_stats_percentage_%d_%d.png' % (title_key, init_batch, final_batch))
			plt.close()

def resnet_plainnet_n_trials_diagnostic_plots(stack_depth, num_trials, num_epochs, root_dir):
	""" Creates diagnostic plots for the trained ResNet-N and PlainNet-N models. 

	Parameters
	----------

	* stack_depth : int 
		An integer specifying the depth of the Plain/Residual blocks in our networks.

	* num_trials : int
		An integer specifying the number of trials performed.

	* num_epochs : int 
		An integer specifying the number of epochs each model was trained for.

	* root_dir : string
		A string specifying the root directory. Will have the following hierarchy:
			- graphics/
			- resnet/
				- 1/
				- .
				- .
				- num_trials/
			- plainnet/
				- 1/
				- .
				- .
				- num_trials/

	"""

	# Verify that the train_snapshot.pkl files exists in the correct directory structure:
	for model in ['resnet/', 'plainnet/']:
		model_dir = root_dir+model
		assert os.path.exists(model_dir) == True, '%s directory does not exist.' % (model_dir)
		for folder in  ['%d/' % (i) for i in xrange(1,num_trials)]:
			assert os.path.exists(model_dir+folder) == True, '%s directory does not exist' % (model_dir+folder)
			assert os.path.exists(model_dir+folder+'train_snapshot.pkl') == True, '%s directory does not contain train_snapshot.pkl diagnostic file.' % (model_dir+folder)

	###################################
	##### Create diagnostic plots #####
	###################################

	# Create a diagnostic OrderedDict:
	diagnostic_dict = OrderedDict()
	diagnostic_dict['resnet'], diagnostic_dict['plainnet'] = OrderedDict(), OrderedDict()
	diagnostic_keys = ['train_loss_epoch_arr', 'test_loss_epoch_arr', 'test_acc_epoch_arr']
	for key in diagnostic_keys:
		diagnostic_dict['resnet'][key] = np.zeros((num_trials, num_epochs))
		diagnostic_dict['plainnet'][key] = np.zeros((num_trials, num_epochs))

	for model in ['resnet/', 'plainnet/']:
		model_dir = root_dir+model
		for index, folder in  enumerate(['%d/' % (i) for i in xrange(1,num_trials+1)]):
			D = pickle.load(open(model_dir+folder+'train_snapshot.pkl','rb'))
			for key in diagnostic_keys:
				if model == 'resnet/':
					diagnostic_dict['resnet'][key][index] = D[key][0:num_epochs]
				elif model == 'plainnet/':
					diagnostic_dict['plainnet'][key][index] = D[key][0:num_epochs]

	# Create a train loss diagnostic plot:
	resnet_train_mean = diagnostic_dict['resnet']['train_loss_epoch_arr'].mean(axis=0)
	resnet_train_std = diagnostic_dict['resnet']['train_loss_epoch_arr'].std(axis=0)
	plainnet_train_mean = diagnostic_dict['plainnet']['train_loss_epoch_arr'].mean(axis=0)
	plainnet_train_std = diagnostic_dict['plainnet']['train_loss_epoch_arr'].std(axis=0)
	ind = np.arange(1, num_epochs+1)
	plt.plot(ind, resnet_train_mean, 'k', label='Res', color='#CC4F1B')
	plt.fill_between(ind, resnet_train_mean-resnet_train_std , resnet_train_mean+resnet_train_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(ind, plainnet_train_mean, 'k', label='Plain', color='#1B2ACC')
	plt.fill_between(ind, plainnet_train_mean-plainnet_train_std, plainnet_train_mean+plainnet_train_std, alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')
	plt.xlabel('Epoch Number')
	plt.ylabel('Loss')
	plt.title('Stack-depth %d Train Loss Per Epoch Averaged Over %d Trials' % (stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='upper right')
	plt.savefig(root_dir+'graphics/train_loss.png')
	plt.close()

	# Create a test loss diagnostic plot:
	resnet_test_mean = diagnostic_dict['resnet']['test_loss_epoch_arr'].mean(axis=0)
	resnet_test_std = diagnostic_dict['resnet']['test_loss_epoch_arr'].std(axis=0)
	plainnet_test_mean = diagnostic_dict['plainnet']['test_loss_epoch_arr'].mean(axis=0)
	plainnet_test_std = diagnostic_dict['plainnet']['test_loss_epoch_arr'].std(axis=0)
	ind = np.arange(1, num_epochs+1)
	plt.plot(ind, resnet_test_mean, 'k', label='Res', color='#CC4F1B')
	plt.fill_between(ind, resnet_test_mean-resnet_test_std , resnet_test_mean+resnet_test_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(ind, plainnet_test_mean, 'k', label='Plain', color='#1B2ACC')
	plt.fill_between(ind, plainnet_test_mean-plainnet_test_std, plainnet_test_mean+plainnet_test_std, alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')
	plt.xlabel('Epoch Number')
	plt.ylabel('Loss')
	plt.title('Stack-depth %d Test Loss Per Epoch Averaged Over %d Trials' % (stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='upper right')
	plt.savefig(root_dir+'graphics/test_loss.png')
	plt.close()

	# Create a test acc diagnostic plot:
	resnet_test_mean = diagnostic_dict['resnet']['test_acc_epoch_arr'].mean(axis=0)
	resnet_test_std = diagnostic_dict['resnet']['test_acc_epoch_arr'].std(axis=0)
	plainnet_test_mean = diagnostic_dict['plainnet']['test_acc_epoch_arr'].mean(axis=0)
	plainnet_test_std = diagnostic_dict['plainnet']['test_acc_epoch_arr'].std(axis=0)
	ind = np.arange(1, num_epochs+1)
	plt.plot(ind, 100*resnet_test_mean, 'k', label='Res', color='#CC4F1B')
	plt.fill_between(ind, 100*resnet_test_mean-100*resnet_test_std, 100*resnet_test_mean+100*resnet_test_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(ind, 100*plainnet_test_mean, 'k', label='Plain', color='#1B2ACC')
	plt.fill_between(ind, 100*plainnet_test_mean-100*plainnet_test_std, 100*plainnet_test_mean+100*plainnet_test_std, alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')
	plt.xlabel('Epoch Number')
	plt.ylabel('Accuracy %')
	plt.title('Stack-depth %d Test Acc Per Epoch Averaged Over %d Trials' % (stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='lower right')
	plt.savefig(root_dir+'graphics/test_acc.png')
	plt.close()

def mimic_net_experiments(data_dir, num_trials, stack_depth, build_teacher_net, build_student_net, root_dir, num_teacher_epochs=85, num_student_epochs=85, teacher_lr=0.1, student_lr=0.001, decay_dict=OrderedDict(), num_classes=10, test_batchsize=500):
	""" This function will train a teacher model, extract the log probabilities for the train and test set and then train a student model to mimic the log probabilities. The student mimic model is trained using the squared error L2 objective using the momentum snapshot trainer function. 

	Parameters
	----------

	* data_dir : string 
		A string specifying the file directory to CIFAR-10 dataset.

	* num_trials : int
		An integer specifying the number of trials to perform.

	* stack_depth : int 
		An integer specifying the depth of the Plain/Residual blocks in our networks.	

	* build_teacher_net : function 
		A build network function for the teacher net.

	* build_student_net : function 
		A build network function for the student net.

	* root_dir : string
		A string specifying the root directory. Will have the following hierarchy:
			- graphics/
			- resnet_%d/ % (stack_depth)
				- teacher/
				- trial_1/
				- .
				- .
				- trial_5/
			- plainnet_%d/ % (stack_depth)
				- teacher/
				- trial_1/
				- .
				- .
				- trial_5/

	* num_teacher_epochs : int 
		An integer specifying the number of epoch to train the teacher net. Default is set to 85. 

	* num_student_epochs : int 
		An integer specifying the number of epoch to train the teacher net. Default is set to 85. 

	* teacher_lr : float 
		A float type specifying the learning rate to use to train the teacher net. Default is set to 0.1.

	* student_lr : float 
		A float type specifying the learning rate to use to train the student nets. Default is set to 0.1.

	* decay_dict : OrderedDict
		An OrderedDict type where the keys are the epoch numbers to decay the learning rate and the corresponding values are the lr decay rates. Default is set to 41st and 61st epochs with a uniform decay rate of 0.1 at each epoch. 

	* num_classes: int 
		An integer specifying the number of classes. Default is set to 10.

	* test_batchsize: int 
		An integer specifying the size of each test batch. Default is set to 500.

	"""
	
	# Create/verify the directory structure: 
	for model in ['resnet', 'plainnet']:
		model_dir = root_dir+model+'_%d/' % stack_depth
		if os.path.exists(model_dir) == False:
			os.makedirs(model_dir)
		for folder in  ['teacher/', 'graphics/'] + ['trial_%d/' % (i) for i in xrange(1,num_trials+1)]:
			if os.path.exists(model_dir+folder) == False:
				os.makedirs(model_dir+folder)

	# Load the CIFAR-10 data:
	print "Loading the CIFAR-10 dataset and processing it..."
	X_train, Y_train, X_val, Y_val, X_test, Y_test = cifar10_load(dir_path=data_dir, num_val=0) 
	X_train, X_val, X_test, mean_img, std_img = gcn(X_train, X_val, X_test, True)
	X_train = np.concatenate((X_train, flips(X_train, 'horizontal')))
	Y_train = np.concatenate((Y_train, Y_train)) 

	data_dict = OrderedDict()
	data_dict['X_train'] = X_train 
	data_dict['Y_train'] = Y_train
	data_dict['X_test'] = X_test 
	data_dict['Y_test'] = Y_test 

	# Initialize Theano symbolic variables:
	input_var = T.tensor4('inputs')
	target_var = T.ivector('targets')

	# Compile teacher_net: 
	teacher_net = build_teacher_net(stack_depth=stack_depth, input_var=input_var)

	if build_teacher_net == build_deep_resnet:
		model_dir = root_dir+'resnet_%d/' % (stack_depth)
		model = 'resnet'
	elif build_teacher_net == build_deep_plainnet:
		model_dir = root_dir+'plainnet_%d/' % (stack_depth)
		model = 'plainnet'

	print 'Training a %s-%d teacher model...' % (model, stack_depth)
	teacher_dir = model_dir+'teacher/' 
	if os.path.isfile(teacher_dir+'train_snapshot.pkl') == True:
		print "Teacher model already trained, skipping to mimic trials..."

		# Load the teacher net snapshot:
		teacher_net_snapshot = pickle.load(open(teacher_dir+'train_snapshot.pkl', 'rb'))
	else: 
		teacher_net_snapshot = momentum_snapshot_trainer(
									net=teacher_net, 
									input_var=input_var, 
									target_var=target_var, 
									data_dict=data_dict, 
									save_iter=100, 
									save_dir=teacher_dir, 
									num_epochs=num_teacher_epochs, 
									batchsize=128, 
									train_continue=False, 
									train_snapshot=None,
									lr=teacher_lr,
									test_batchsize=test_batchsize)

	# Load the maximimal performance weights: 
	max_test_acc_batch_index = np.where(teacher_net_snapshot['test_acc_arr'] == teacher_net_snapshot['test_acc_arr'].max())[0][0]*100+100
	best_test_acc = teacher_net_snapshot['test_acc_arr'].max()
	with np.load(teacher_dir+'%d_iter_completed_weights.npz' % (max_test_acc_batch_index)) as f:
		best_test_acc_weights = [f['arr_%d' % i] for i in range(len(f.files))]
	set_all_param_values(teacher_net['logits'], best_test_acc_weights)

	# Extract the training logits from the teacher_net:
	print "Extracting logits from the teacher_net..."
	teacher_logits = get_output(teacher_net['logits'], deterministic=True)
	get_teacher_logits = theano.function([input_var], teacher_logits)
	Y_train_logits = np.zeros((0, num_classes))
	for inputs, targets in iterate_minibatches(X_train, Y_train, batchsize=500, shuffle=False):
		logits_batch = get_teacher_logits(inputs)
		Y_train_logits = np.concatenate((Y_train_logits, logits_batch),axis=0)

	# Extract the test logits from the teacher_net:
	Y_test_logits = np.zeros((0, num_classes))
	for inputs, targets in iterate_minibatches(X_test, Y_test, batchsize=500, shuffle=False):
		logits_batch = get_teacher_logits(inputs)
		Y_test_logits = np.concatenate((Y_test_logits, logits_batch),axis=0)

	Y_train_logits = Y_train_logits.astype(theano.config.floatX)
	Y_test_logits = Y_test_logits.astype(theano.config.floatX)

	# Create a data_dict for mimic training:
	logit_data_dict = OrderedDict()
	logit_data_dict['X_train'] = X_train 
	logit_data_dict['Y_train'] = Y_train 
	logit_data_dict['Y_train_logits'] = Y_train_logits 
	logit_data_dict['X_test'] = X_test 
	logit_data_dict['Y_test'] = Y_test 
	logit_data_dict['Y_test_logits'] = Y_test_logits 

	# Train the mimic nets:
	np.random.seed(123456)
	for i in xrange(num_trials):
		print '\t Performing mimic trial %d of %d...' % (i+1, num_trials)
		mimic_dir = model_dir+'trial_%d/' % (i+1)

		if os.path.isfile(mimic_dir+'training_diagnostics.pkl') == True:
			print "Mimic trial %d already completed, skipping to to mimic trial %d..." % (i+1, i+2)
		else:
			# Initialize student_net with the same exact weights:
			student_net = build_student_net(stack_depth=stack_depth, input_var=input_var)

			# Train student_net to mimic teacher_net:
			print "Training student_net to mimic teacher_net..."
			student_net_diagnostics = train_mimic_net(
						save_dir=mimic_dir, 
						student_net=student_net, 
						input_var=input_var, 
						target_var=target_var, 
						data_dict=logit_data_dict, 
						num_epochs=num_student_epochs, 
						update_mode='momentum',
						batchsize=128, 
						reg=0.0, 
						# lr=0.001,
						lr=student_lr, 
						mu=0.9,
						save_iter=100,
						decay_dict=decay_dict,
						test_batchsize=test_batchsize)

	###################################
	##### Create diagnostic plots #####
	###################################

	##################################################################
	### Create a mean L2+std error Train and Test Mimic Error Plot ###
	##################################################################

	# Compile all the L2 errors:
	for i in xrange(num_trials):
		mimic_dir = model_dir+'trial_%d/' % (i+1)
		diagnostic_dict = pickle.load(open(mimic_dir+'training_diagnostics.pkl', 'rb'))
		if i == 0:
			N = diagnostic_dict['train l2 per epoch'].shape[0]
			test_l2_arr = np.zeros((num_trials, N))
			train_l2_arr = np.zeros((num_trials, N))
		test_l2_arr[i] = diagnostic_dict['test l2 per epoch']
		train_l2_arr[i] = diagnostic_dict['train l2 per epoch']

	# Compute the mean and std vals:
	test_l2_mean, test_l2_std = test_l2_arr.mean(axis=0), test_l2_arr.std(axis=0)
	train_l2_mean, train_l2_std = train_l2_arr.mean(axis=0), train_l2_arr.std(axis=0)

	# Create a plot:
	ind = np.arange(1, N+1)
	plt.plot(ind, train_l2_mean, 'k', label='Train', color='#CC4F1B')
	plt.fill_between(ind, train_l2_mean-train_l2_std, train_l2_mean+train_l2_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(ind, test_l2_mean, 'k', label='Test', color='#1B2ACC')
	plt.fill_between(ind, test_l2_mean-test_l2_std, test_l2_mean+test_l2_std, alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')
	plt.xlabel('Epoch Number')
	plt.ylabel('L2 Error')
	plt.title('PlainNet-%d L2 Mimic Per Epoch Error Over %d Trials' % (stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='upper right')
	plt.savefig(model_dir+'graphics/plainnet_%d_l2_mimic.png' % (stack_depth))
	plt.close()

	###############################################################
	### Create train and test softmax per epoch diagnostic plot ###
	###############################################################

	# Compile all the softmax losses:
	for i in xrange(num_trials):
		mimic_dir = model_dir+'trial_%d/' % (i+1)
		diagnostic_dict = pickle.load(open(mimic_dir+'training_diagnostics.pkl', 'rb'))
		if i == 0:
			N = diagnostic_dict['train softmax per epoch'].shape[0]
			test_softmax_arr = np.zeros((num_trials, N))
			train_softmax_arr = np.zeros((num_trials, N))
		test_softmax_arr[i] = diagnostic_dict['test softmax per epoch']
		train_softmax_arr[i] = diagnostic_dict['train softmax per epoch']

	# Compute the mean and std vals:
	test_softmax_mean, test_softmax_std = test_softmax_arr.mean(axis=0), test_softmax_arr.std(axis=0)
	train_softmax_mean, train_softmax_std = train_softmax_arr.mean(axis=0), train_softmax_arr.std(axis=0)

	# Create the train softmax plot:
	student_ind = np.arange(1, N+1)
	teacher_ind = np.arange(1, num_teacher_epochs+1)
	plt.plot(student_ind, train_softmax_mean, 'k', label='Plain-Mimic', color='#CC4F1B')
	plt.fill_between(student_ind, train_softmax_mean-train_softmax_std, train_softmax_mean+train_softmax_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(teacher_ind, teacher_net_snapshot['train_loss_epoch_arr'], 'k', label='Res-Teacher', color='blue')
	plt.xlabel('Epoch Number')
	plt.ylabel('Softmax Loss')
	plt.title('Softmax Train Loss per Epoch\nResNet-%d Teacher vs. PlainNet-%d Mimic Averaged Over %d Trials' % (stack_depth, stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='upper right')
	plt.savefig(model_dir+'graphics/plainnet_%d_softmax_train.png' % (stack_depth))
	plt.close()

	# Create the test softmax plot:
	student_ind = np.arange(1, N+1)
	teacher_ind = np.arange(1, num_teacher_epochs+1)
	plt.plot(student_ind, test_softmax_mean, 'k', label='Plain-Mimic', color='#CC4F1B')
	plt.fill_between(student_ind, test_softmax_mean-test_softmax_std, test_softmax_mean+test_softmax_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(teacher_ind, teacher_net_snapshot['test_loss_epoch_arr'], 'k', label='Res-Teacher', color='blue')
	plt.xlabel('Epoch Number')
	plt.ylabel('Softmax Loss')
	plt.title('Softmax Test Loss per Epoch\nResNet-%d Teacher vs. PlainNet-%d Mimic Averaged Over %d Trials' % (stack_depth, stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='upper right')
	plt.savefig(model_dir+'graphics/plainnet_%d_softmax_test.png' % (stack_depth))
	plt.close()

	#######################################
	### Create test acc diagnostic plot ###
	#######################################

	# Compile all the test accuracies:
	for i in xrange(num_trials):
		mimic_dir = model_dir+'trial_%d/' % (i+1)
		diagnostic_dict = pickle.load(open(mimic_dir+'training_diagnostics.pkl', 'rb'))
		if i == 0:
			N = diagnostic_dict['test acc per epoch'].shape[0]
			test_acc_arr = np.zeros((num_trials, N))
		test_acc_arr[i] = diagnostic_dict['test acc per epoch']

	# Compute the mean and std vals:
	test_acc_mean, test_acc_std = test_acc_arr.mean(axis=0), test_acc_arr.std(axis=0)

	# Create the test acc plot:
	student_ind = np.arange(1, N+1)
	teacher_ind = np.arange(1, num_teacher_epochs+1)
	plt.plot(student_ind, 100*test_acc_mean, 'k', label='Plain-Mimic', color='#CC4F1B')
	plt.fill_between(student_ind, 100*test_acc_mean-100*test_acc_std, 100*test_acc_mean+100*test_acc_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
	plt.plot(teacher_ind, 100*teacher_net_snapshot['test_acc_epoch_arr'], 'k', label='Res-Teacher', color='blue')
	plt.axhline(y=100*best_test_acc, c='purple', ls='--', label='Best Res Test Acc')
	plt.xlabel('Epoch Number')
	plt.ylabel('Accuracy %')
	plt.ylim(0,100)
	plt.title('Test Accuracy per Epoch\nResNet-%d Teacher vs. PlainNet-%d Mimic Averaged Over %d Trials' % (stack_depth, stack_depth, num_trials))
	plt.tight_layout()
	plt.legend(loc='lower right')
	plt.savefig(model_dir+'graphics/plainnet_%d_test_acc.png' % (stack_depth))
	plt.close()

def layer_wise_lr_decay_cifar10_experiment(root_dir, data_dir, stack_depth, num_trials=1, num_epochs=60, lr_decay_epoch=40):
	""" Performs the layer-wise-learning-rate-decay experiments for training on the CIFAR-10 dataset. We use three different types of learning rate decay schemes:

			(1) No LR decay.
			(2) Uniform LR decay at a certain epoch.
			(3) RHT-LR-Decay at a certain epoch.

	Parameters
	----------

	* root_dir : string
		A string specifying the root directory. Will have the following hierarchy:
			- graphics/
			- resnet/
				- trial_1/
					- init/
					- no_lr_decay/
					- uniform_lr_decay/
					- rht_lr_decay/
				- .
				- .
				- trial_%d/ % (num_trials)
					- init/
					- no_lr_decay/
					- uniform_lr_decay/
					- rht_lr_decay/
			- plainnet/
				- trial_1/
					- init/
					- no_lr_decay/
					- uniform_lr_decay/
					- rht_lr_decay/
				- .
				- .
				- trial_%d/ % (num_trials)
					- init/
					- no_lr_decay/
					- uniform_lr_decay/
					- rht_lr_decay/

	* data_dir : string 
		A string specifying the file directory to CIFAR-10 dataset.

	* stack_depth : int 
		An integer specifying the depth of the Plain/Residual blocks in our networks.

	* num_trials : int
		An integer specifying the number of trials to perform. Default is set to 1.

	* num_epochs : int
		An integer specifying the total number of epochs to train. Default is set to 60.

	* lr_decay_epoch : int 
		An integer specifying the epoch at which we will decay the LR. Dfault is set to 40.

	"""

	# Create/verify the directory structure: 
	lr_decay_dirs = ['init/', 'no_lr_decay/', 'uniform_lr_decay/', 'rht_lr_decay/']
	for model in ['resnet/', 'plainnet/']:
		for trial in ['trial_%d/' % (i) for i in xrange(1,num_trials+1)]:
			for lr_decay_dir in lr_decay_dirs:
				current_dir = root_dir+model+trial+lr_decay_dir
				if os.path.exists(current_dir) == False:
					os.makedirs(current_dir)
	if os.path.exists(root_dir+'graphics/') == False:
		os.makedirs(root_dir+'graphics/')

	# Load the CIFAR-10 data and process it: 
	print "Loading the CIFAR-10 dataset and processing it..."
	X_train, Y_train, X_val, Y_val, X_test, Y_test = cifar10_load(dir_path=data_dir, num_val=0) 
	X_train, X_val, X_test, mean_img, std_img = gcn(X_train, X_val, X_test, True)
	X_train = np.concatenate((X_train, flips(X_train, 'horizontal')))
	Y_train = np.concatenate((Y_train, Y_train)) 

	data_dict = OrderedDict()
	data_dict['X_train'] = X_train 
	data_dict['Y_train'] = Y_train
	data_dict['X_test'] = X_test 
	data_dict['Y_test'] = Y_test 

	# Initialize Theano symbolic variables:
	input_var = T.tensor4('inputs')
	target_var = T.ivector('targets')

	# Define the uniform LR decay dict:
	uniform_decay_dict = OrderedDict()
	uniform_decay_dict[lr_decay_epoch] = 0.1

	# Define the non-uniform RHT LR decay rates:
	net = build_deep_resnet(stack_depth=stack_depth, input_var=input_var)
	all_trainable_params = get_all_params(net['softmax'], trainable=True)
	rht_lr_rates = np.linspace(np.log(0.001), np.log(0.01), len(all_trainable_params))
	rht_lr_rates = np.exp(rht_lr_rates[::-1]).astype(theano.config.floatX)
	
	del net, all_trainable_params

	for i in xrange(1,num_trials+1):
		# Initialize networks:
		resnet = build_deep_resnet(input_var=input_var, stack_depth=stack_depth)
		plainnet = build_deep_plainnet(input_var=input_var, stack_depth=stack_depth)

		# Define resnet and plainnet param directories:
		resnet_param_dir = root_dir+'resnet/trial_%d/' % (i)
		plainnet_param_dir = root_dir+'plainnet/trial_%d/' % (i)

		###########################################################
		##### Perform the LR decay experiments for the ResNet #####
		###########################################################
		print "Performing the LR decay experiments for the ResNet-%d..." % (stack_depth)

		# Train the ResNet for (lr_decay_epoch) number of epochs:
		print "\t Training for %d epochs with initial settings..." % (lr_decay_epoch)
		if os.path.isfile(resnet_param_dir+'init/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."

			# Load the snapshot:
			lr_decay_epoch_snapshot = pickle.load(open(resnet_param_dir+'init/train_snapshot.pkl', 'rb'))
		else:
			lr_decay_epoch_snapshot = momentum_snapshot_trainer(
										net=resnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100,
										save_dir=resnet_param_dir+'init/', 
										num_epochs=lr_decay_epoch, 
										batchsize=128, 
										train_continue=False, 
										train_snapshot=None, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=False, 
										layer_wise_lr_rates=None, 
										uniform_decay_dict=OrderedDict()) # No decay

		# Train the ResNet for (num_epochs-lr_decay_epoch) number of epochs with no LR decay:
		print "\t Training for %d epochs with no LR decay..." % (num_epochs-lr_decay_epoch)
		if os.path.isfile(resnet_param_dir+'no_lr_decay/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."
		else:
			no_lr_decay_snapshot = momentum_snapshot_trainer(
										net=resnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100,
										save_dir=resnet_param_dir+'no_lr_decay/', 
										num_epochs=num_epochs-lr_decay_epoch, 
										batchsize=128, 
										train_continue=True, 
										train_snapshot=lr_decay_epoch_snapshot, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=False, 
										layer_wise_lr_rates=None,
										uniform_decay_dict=OrderedDict()) # No decay

		# Train the ResNet for (num_epochs-lr_decay_epoch) number of epochs with uniform LR decay:
		print "\t Training for %d epochs with uniform LR decay..." % (num_epochs-lr_decay_epoch)
		if os.path.isfile(resnet_param_dir+'uniform_lr_decay/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."
		else:
			uniform_lr_decay_snapshot = momentum_snapshot_trainer(
										net=resnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100, 
										save_dir=resnet_param_dir+'uniform_lr_decay/', 
										num_epochs=num_epochs-lr_decay_epoch, 
										batchsize=128, 
										train_continue=True, 
										train_snapshot=lr_decay_epoch_snapshot, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=False, 
										layer_wise_lr_rates=None,
										uniform_decay_dict=uniform_decay_dict)

		# Train the ResNet for (num_epochs-lr_decay_epoch) number of epochs with RHT LR decay:
		print "\t Training for %d epochs with RHT LR decay..." % (num_epochs-lr_decay_epoch)
		if os.path.isfile(resnet_param_dir+'rht_lr_decay/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."
		else:
			rht_lr_decay_snapshot = momentum_snapshot_trainer(
										net=resnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100, 
										save_dir=resnet_param_dir+'rht_lr_decay/', 
										num_epochs=num_epochs-lr_decay_epoch, 
										batchsize=128, 
										train_continue=True, 
										train_snapshot=lr_decay_epoch_snapshot, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=True, 
										layer_wise_lr_rates=rht_lr_rates,
										uniform_decay_dict=OrderedDict()) 

		#############################################################
		##### Perform the LR decay experiments for the PlainNet #####
		#############################################################
		print "Performing the LR decay experiments for the PlainNet-%d..." % (stack_depth)
		# Train the PlainNet for (lr_decay_epoch) number of epochs:
		if os.path.isfile(plainnet_param_dir+'init/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."

			# Load the snapshot:
			lr_decay_epoch_snapshot = pickle.load(open(plainnet_param_dir+'init/train_snapshot.pkl', 'rb'))
		else:
			lr_decay_epoch_snapshot = momentum_snapshot_trainer(
										net=plainnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100,
										save_dir=plainnet_param_dir+'init/',
										num_epochs=lr_decay_epoch, 
										batchsize=128, 
										train_continue=False, 
										train_snapshot=None, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=False, 
										layer_wise_lr_rates=None, 
										uniform_decay_dict=OrderedDict()) # No decay

		# Train the PlainNet for (num_epochs-lr_decay_epoch) number of epochs more epochs with no LR decay:
		print "\t Training for %d epochs with no LR decay..." % (num_epochs-lr_decay_epoch)
		if os.path.isfile(plainnet_param_dir+'no_lr_decay/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."
		else:
			no_lr_decay_snapshot = momentum_snapshot_trainer(
										net=plainnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100,
										save_dir=plainnet_param_dir+'no_lr_decay/', 
										num_epochs=num_epochs-lr_decay_epoch, 
										batchsize=128, 
										train_continue=True, 
										train_snapshot=lr_decay_epoch_snapshot, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=False, 
										layer_wise_lr_rates=None,
										uniform_decay_dict=OrderedDict()) # No decay

		# Train the PlainNet for (num_epochs-lr_decay_epoch) number of epochs more epochs with uniform LR decay:
		print "\t Training for %d epochs with uniform LR decay..." % (num_epochs-lr_decay_epoch)
		if os.path.isfile(plainnet_param_dir+'uniform_lr_decay/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."
		else:
			uniform_lr_decay_snapshot = momentum_snapshot_trainer(
										net=plainnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100, 
										save_dir=plainnet_param_dir+'uniform_lr_decay/', 
										num_epochs=num_epochs-lr_decay_epoch, 
										batchsize=128, 
										train_continue=True, 
										train_snapshot=lr_decay_epoch_snapshot, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=False, 
										layer_wise_lr_rates=None,
										uniform_decay_dict=uniform_decay_dict) 

		# Train the PlainNet for (num_epochs-lr_decay_epoch) number of epochs more epochs with conv+ LR decay:
		print "\t Training for %d epochs with RHT LR decay..." % (num_epochs-lr_decay_epoch)
		if os.path.isfile(plainnet_param_dir+'rht_lr_decay/train_snapshot.pkl') == True:
			print "\tAlready trained, passing..."
		else:
			rht_lr_decay_snapshot = momentum_snapshot_trainer(
										net=plainnet, 
										input_var=input_var, 
										target_var=target_var, 
										data_dict=data_dict, 
										save_iter=100, 
										save_dir=plainnet_param_dir+'rht_lr_decay/', 
										num_epochs=num_epochs-lr_decay_epoch, 
										batchsize=128, 
										train_continue=True, 
										train_snapshot=lr_decay_epoch_snapshot, 
										reg=0.0001, 
										lr=0.1, 
										mu=0.9, 
										layer_wise_lr=True, 
										layer_wise_lr_rates=rht_lr_rates,
										uniform_decay_dict=OrderedDict()) 

	####################################
	##### Produce Diagnostic Plots #####
	####################################

	# Define diagnostic OrderedDicts:
	lr_decay_types = ['no_lr_decay', 'uniform_lr_decay', 'rht_lr_decay']
	diagnostic_keys = ['train_loss', 'test_loss', 'test_acc']

	res_dict = OrderedDict()
	for lr_decay_type in lr_decay_types:
		res_dict[lr_decay_type] = OrderedDict()
		for key in diagnostic_keys:
			res_dict[lr_decay_type][key] = OrderedDict()
			res_dict[lr_decay_type][key]['values'] = np.zeros((num_trials, num_epochs))
			res_dict[lr_decay_type][key]['mean'] = np.zeros((num_epochs))
			res_dict[lr_decay_type][key]['std'] = np.zeros((num_epochs))

	plain_dict = OrderedDict()
	for lr_decay_type in lr_decay_types:
		plain_dict[lr_decay_type] = OrderedDict()
		for key in diagnostic_keys:
			plain_dict[lr_decay_type][key] = OrderedDict()
			plain_dict[lr_decay_type][key]['values'] = np.zeros((num_trials, num_epochs))
			plain_dict[lr_decay_type][key]['mean'] = np.zeros((num_epochs))
			plain_dict[lr_decay_type][key]['std'] = np.zeros((num_epochs))

	# Define plot indices for the no LR decay and for the decay models:
	ind = np.arange(1, num_epochs+1)
	decay_ind = np.arange(lr_decay_epoch+1, num_epochs+1)

	#############################################
	### Create Res and Plain Train Loss Plots ###
	#############################################

	# Compute the ResNet train loss mean and std:
	for i in xrange(num_trials):
		for lr_decay_type in lr_decay_types:
			D = pickle.load(open(os.path.join(root_dir,'resnet/trial_%d' % (i+1), lr_decay_type, 'train_snapshot.pkl'), 'rb'))
			res_dict[lr_decay_type]['train_loss']['values'][i] = D['train_loss_epoch_arr']

	for lr_decay_type in lr_decay_types:
		res_dict[lr_decay_type]['train_loss']['mean'] = res_dict[lr_decay_type]['train_loss']['values'].mean(axis=0)
		res_dict[lr_decay_type]['train_loss']['std'] = res_dict[lr_decay_type]['train_loss']['values'].std(axis=0)

	# No LR-Decay Plot:
	plt.plot(ind, res_dict['no_lr_decay']['train_loss']['mean'], 'k', label='No LR-Decay', color='#CC4F1B')
	plt.fill_between(ind, res_dict['no_lr_decay']['train_loss']['mean']-res_dict['no_lr_decay']['train_loss']['std'], res_dict['no_lr_decay']['train_loss']['mean']+res_dict['no_lr_decay']['train_loss']['std'], alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

	# Uniform Decay Plot:
	plt.plot(decay_ind, res_dict['uniform_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='Uniform LR-Decay', color='#1B2ACC')
	plt.fill_between(decay_ind, res_dict['uniform_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]-res_dict['uniform_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], res_dict['uniform_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]+res_dict['uniform_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')

	# RHT Decay Plot:
	plt.plot(decay_ind, res_dict['rht_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='RHT LR-Decay', color='#EE00EE')
	plt.fill_between(decay_ind, res_dict['rht_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]-res_dict['rht_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], res_dict['rht_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]+res_dict['rht_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#EE00EE', facecolor='#BDA0CB')
	plt.xlabel('Epoch Number')
	plt.ylabel('Loss')
	plt.title('ResNet-%d Train Loss: LR Decay' % (stack_depth))
	plt.legend(loc='upper right')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/resnet_%d_train_loss.png' % (stack_depth))
	plt.close()

	# Compute the PlainNet train loss mean and std:
	for i in xrange(num_trials):
		for lr_decay_type in lr_decay_types:
			D = pickle.load(open(os.path.join(root_dir,'plainnet/trial_%d' % (i+1), lr_decay_type, 'train_snapshot.pkl'), 'rb'))
			plain_dict[lr_decay_type]['train_loss']['values'][i] = D['train_loss_epoch_arr']

	for lr_decay_type in lr_decay_types:
		plain_dict[lr_decay_type]['train_loss']['mean'] = plain_dict[lr_decay_type]['train_loss']['values'].mean(axis=0)
		plain_dict[lr_decay_type]['train_loss']['std'] = plain_dict[lr_decay_type]['train_loss']['values'].std(axis=0)

	# No LR-Decay Plot:
	plt.plot(ind, plain_dict['no_lr_decay']['train_loss']['mean'], 'k', label='No LR-Decay', color='#CC4F1B')
	plt.fill_between(ind, plain_dict['no_lr_decay']['train_loss']['mean']-plain_dict['no_lr_decay']['train_loss']['std'], plain_dict['no_lr_decay']['train_loss']['mean']+plain_dict['no_lr_decay']['train_loss']['std'], alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

	# Uniform Decay Plot:
	plt.plot(decay_ind, plain_dict['uniform_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='Uniform LR-Decay', color='#1B2ACC')
	plt.fill_between(decay_ind, plain_dict['uniform_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]-plain_dict['uniform_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], plain_dict['uniform_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]+plain_dict['uniform_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')

	# RHT Decay Plot:
	plt.plot(decay_ind, plain_dict['rht_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='RHT LR-Decay', color='#EE00EE')
	plt.fill_between(decay_ind, plain_dict['rht_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]-plain_dict['rht_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], plain_dict['rht_lr_decay']['train_loss']['mean'][lr_decay_epoch:num_epochs]+plain_dict['rht_lr_decay']['train_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#EE00EE', facecolor='#BDA0CB')
	plt.xlabel('Epoch Number')
	plt.ylabel('Loss')
	plt.title('PlainNet-%d Train Loss: LR Decay' % (stack_depth))
	plt.legend(loc='upper right')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/plainnet_%d_train_loss.png' % (stack_depth))
	plt.close()

	############################################
	### Create Res and Plain Test Loss Plots ###
	############################################

	# Compute the resnet test loss mean and std:
	for i in xrange(num_trials):
		for lr_decay_type in lr_decay_types:
			D = pickle.load(open(os.path.join(root_dir,'resnet/trial_%d' % (i+1), lr_decay_type, 'train_snapshot.pkl'), 'rb'))
			res_dict[lr_decay_type]['test_loss']['values'][i] = D['test_loss_epoch_arr']

	for lr_decay_type in lr_decay_types:
		res_dict[lr_decay_type]['test_loss']['mean'] = res_dict[lr_decay_type]['test_loss']['values'].mean(axis=0)
		res_dict[lr_decay_type]['test_loss']['std'] = res_dict[lr_decay_type]['test_loss']['values'].std(axis=0)

	# No LR-Decay Plot:
	plt.plot(ind, res_dict['no_lr_decay']['test_loss']['mean'], 'k', label='No LR-Decay', color='#CC4F1B')
	plt.fill_between(ind, res_dict['no_lr_decay']['test_loss']['mean']-res_dict['no_lr_decay']['test_loss']['std'], res_dict['no_lr_decay']['test_loss']['mean']+res_dict['no_lr_decay']['test_loss']['std'], alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

	# Uniform Decay Plot:
	plt.plot(decay_ind, res_dict['uniform_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='Uniform LR-Decay', color='#1B2ACC')
	plt.fill_between(decay_ind, res_dict['uniform_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]-res_dict['uniform_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], res_dict['uniform_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]+res_dict['uniform_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')

	# RHT Decay Plot:
	plt.plot(decay_ind, res_dict['rht_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='RHT LR-Decay', color='#EE00EE')
	plt.fill_between(decay_ind, res_dict['rht_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]-res_dict['rht_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], res_dict['rht_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]+res_dict['rht_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#EE00EE', facecolor='#BDA0CB')
	plt.xlabel('Epoch Number')
	plt.ylabel('Loss')
	plt.title('ResNet-%d Test Loss: LR Decay' % (stack_depth))
	plt.legend(loc='upper right')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/resnet_%d_test_loss.png' % (stack_depth))
	plt.close()

	# Compute the PlainNet test loss mean and std:
	for i in xrange(num_trials):
		for lr_decay_type in lr_decay_types:
			D = pickle.load(open(os.path.join(root_dir,'plainnet/trial_%d' % (i+1), lr_decay_type, 'train_snapshot.pkl'), 'rb'))
			plain_dict[lr_decay_type]['test_loss']['values'][i] = D['test_loss_epoch_arr']

	for lr_decay_type in lr_decay_types:
		plain_dict[lr_decay_type]['test_loss']['mean'] = plain_dict[lr_decay_type]['test_loss']['values'].mean(axis=0)
		plain_dict[lr_decay_type]['test_loss']['std'] = plain_dict[lr_decay_type]['test_loss']['values'].std(axis=0)

	# No LR-Decay Plot:
	plt.plot(ind, plain_dict['no_lr_decay']['test_loss']['mean'], 'k', label='No LR-Decay', color='#CC4F1B')
	plt.fill_between(ind, plain_dict['no_lr_decay']['test_loss']['mean']-plain_dict['no_lr_decay']['test_loss']['std'], plain_dict['no_lr_decay']['test_loss']['mean']+plain_dict['no_lr_decay']['test_loss']['std'], alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

	# Uniform Decay Plot:
	plt.plot(decay_ind, plain_dict['uniform_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='Uniform LR-Decay', color='#1B2ACC')
	plt.fill_between(decay_ind, plain_dict['uniform_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]-plain_dict['uniform_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], plain_dict['uniform_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]+plain_dict['uniform_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')

	# RHT Decay Plot:
	plt.plot(decay_ind, plain_dict['rht_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs], 'k', label='RHT LR-Decay', color='#EE00EE')
	plt.fill_between(decay_ind, plain_dict['rht_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]-plain_dict['rht_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], plain_dict['rht_lr_decay']['test_loss']['mean'][lr_decay_epoch:num_epochs]+plain_dict['rht_lr_decay']['test_loss']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#EE00EE', facecolor='#BDA0CB')
	plt.xlabel('Epoch Number')
	plt.ylabel('Loss')
	plt.title('PlainNet-%d Test Loss: LR Decay' % (stack_depth))
	plt.legend(loc='upper right')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/plainnet_%d_test_loss.png' % (stack_depth))
	plt.close()

	###########################################
	### Create Res and Plain Test Acc Plots ###
	###########################################

	# Compute the resnet test acc mean and std:
	for i in xrange(num_trials):
		for lr_decay_type in lr_decay_types:
			D = pickle.load(open(os.path.join(root_dir,'resnet/trial_%d' % (i+1), lr_decay_type, 'train_snapshot.pkl'), 'rb'))
			res_dict[lr_decay_type]['test_acc']['values'][i] = D['test_acc_epoch_arr']

	for lr_decay_type in lr_decay_types:
		res_dict[lr_decay_type]['test_acc']['mean'] = 100*res_dict[lr_decay_type]['test_acc']['values'].mean(axis=0)
		res_dict[lr_decay_type]['test_acc']['std'] = 100*res_dict[lr_decay_type]['test_acc']['values'].std(axis=0)

	# No LR-Decay Plot:
	plt.plot(ind, res_dict['no_lr_decay']['test_acc']['mean'], 'k', label='No LR-Decay', color='#CC4F1B')
	plt.fill_between(ind, res_dict['no_lr_decay']['test_acc']['mean']-res_dict['no_lr_decay']['test_acc']['std'], res_dict['no_lr_decay']['test_acc']['mean']+res_dict['no_lr_decay']['test_acc']['std'], alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

	# Uniform Decay Plot:
	plt.plot(decay_ind, res_dict['uniform_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs], 'k', label='Uniform LR-Decay', color='#1B2ACC')
	plt.fill_between(decay_ind, res_dict['uniform_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]-res_dict['uniform_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], res_dict['uniform_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]+res_dict['uniform_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')

	# RHT Decay Plot:
	plt.plot(decay_ind, res_dict['rht_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs], 'k', label='RHT LR-Decay', color='#EE00EE')
	plt.fill_between(decay_ind, res_dict['rht_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]-res_dict['rht_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], res_dict['rht_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]+res_dict['rht_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#EE00EE', facecolor='#BDA0CB')
	plt.xlabel('Epoch Number')
	plt.ylabel('Accuracy %')
	plt.title('ResNet-%d Test Loss: LR Decay' % (stack_depth))
	plt.legend(loc='lower right')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/resnet_%d_test_acc.png' % (stack_depth))
	plt.close()

	# Compute the PlainNet test acc mean and std:
	for i in xrange(num_trials):
		for lr_decay_type in lr_decay_types:
			D = pickle.load(open(os.path.join(root_dir,'plainnet/trial_%d' % (i+1), lr_decay_type, 'train_snapshot.pkl'), 'rb'))
			plain_dict[lr_decay_type]['test_acc']['values'][i] = D['test_acc_epoch_arr']

	for lr_decay_type in lr_decay_types:
		plain_dict[lr_decay_type]['test_acc']['mean'] = 100*plain_dict[lr_decay_type]['test_acc']['values'].mean(axis=0)
		plain_dict[lr_decay_type]['test_acc']['std'] = 100*plain_dict[lr_decay_type]['test_acc']['values'].std(axis=0)

	# No LR-Decay Plot:
	plt.plot(ind, plain_dict['no_lr_decay']['test_acc']['mean'], 'k', label='No LR-Decay', color='#CC4F1B')
	plt.fill_between(ind, plain_dict['no_lr_decay']['test_acc']['mean']-plain_dict['no_lr_decay']['test_acc']['std'], plain_dict['no_lr_decay']['test_acc']['mean']+plain_dict['no_lr_decay']['test_acc']['std'], alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

	# Uniform Decay Plot:
	plt.plot(decay_ind, plain_dict['uniform_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs], 'k', label='Uniform LR-Decay', color='#1B2ACC')
	plt.fill_between(decay_ind, plain_dict['uniform_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]-plain_dict['uniform_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], plain_dict['uniform_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]+plain_dict['uniform_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')

	# RHT Decay Plot:
	plt.plot(decay_ind, plain_dict['rht_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs], 'k', label='RHT LR-Decay', color='#EE00EE')
	plt.fill_between(decay_ind, plain_dict['rht_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]-plain_dict['rht_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], plain_dict['rht_lr_decay']['test_acc']['mean'][lr_decay_epoch:num_epochs]+plain_dict['rht_lr_decay']['test_acc']['std'][lr_decay_epoch:num_epochs], alpha=0.5, edgecolor='#EE00EE', facecolor='#BDA0CB')
	plt.xlabel('Epoch Number')
	plt.ylabel('Accuracy %')
	plt.title('PlainNet-%d Test Acc: LR Decay' % (stack_depth))
	plt.legend(loc='lower right')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/plainnet_%d_test_acc.png' % (stack_depth))
	plt.close()

if __name__ == '__main__':

	##############################
	##### Define directories #####
	##############################

	exp_dir 	= '/your_experiment_directory/'
	cifar10_dir = '/your_cifar_10_directory/'

	##########################################################
	##### Experiment 1: N Equals 3 Activation Statistics #####
	##########################################################

	# Train 5 ResNet-3 and 5 PlainNet-3 models for 52 epochs each:
	train_n_resnet_and_plainnet(
			data_dir=cifar10_dir, 
			stack_depth=3, 
			num_trials=5, 
			root_dir=exp_dir+'n-equals-3/', 
			save_iter=100, 
			num_epochs=52)

	# Create diagnostic plots:
	resnet_plainnet_n_trials_diagnostic_plots(
		stack_depth=3, 
		num_trials=5, 
		num_epochs=52, 
		root_dir=exp_dir+'n-equals-3/')

	# Use the above 5/5 trained models to perform the activation statistics experiments:
	activation_statistics_cifar10_experiment_n_trials(
			data_dir=cifar10_dir, 
			stack_depth=3, 
			num_trials=5, 
			root_dir=exp_dir+'n-equals-3/', 
			compute_from_scratch=True)

	##########################################################
	##### Experiment 2: N Equals 5 Activation Statistics #####
	##########################################################
	
	# Train 5 ResNet-5 and 5 PlainNet-5 models for 52 epochs each:
	train_n_resnet_and_plainnet(
			data_dir=cifar10_dir, 
			stack_depth=5, 
			num_trials=5, 
			root_dir=exp_dir+'n-equals-5/', 
			save_iter=100, 
			num_epochs=52)

	# Create diagnostic plots:
	resnet_plainnet_n_trials_diagnostic_plots(
		stack_depth=5, 
		num_trials=5, 
		num_epochs=52, 
		root_dir=exp_dir+'n-equals-5/')

	# Use the above 5/5 trained models to perform the activation statistics experiments:
	activation_statistics_cifar10_experiment_n_trials(
			data_dir=cifar10_dir, 
			stack_depth=5, 
			num_trials=5, 
			root_dir=exp_dir+'n-equals-5/', 
			compute_from_scratch=True)

	##########################################################
	##### Experiment 3: N Equals 7 Activation Statistics #####
	##########################################################

	# Train 5 ResNet-7 and 5 PlainNet-7 models for 52 epochs each:
	train_n_resnet_and_plainnet(
			data_dir=cifar10_dir, 
			stack_depth=7, 
			num_trials=5, 
			root_dir=exp_dir+'n-equals-7/', 
			save_iter=100, 
			num_epochs=52)

	# Create diagnostic plots:
	resnet_plainnet_n_trials_diagnostic_plots(
		stack_depth=7, 
		num_trials=5, 
		num_epochs=52, 
		root_dir=exp_dir+'n-equals-7/')

	# Use the above 5/5 trained models to perform the activation statistics experiments:
	activation_statistics_cifar10_experiment_n_trials(
			data_dir=cifar10_dir, 
			stack_depth=7, 
			num_trials=5, 
			root_dir=exp_dir+'n-equals-7/', 
			compute_from_scratch=True)

	##########################################
	##### Experiment 4: N equals 3 Mimic #####
	##########################################

	mimic_net_experiments(
		data_dir=cifar10_dir, 
		num_trials=1, 
		stack_depth=3, 
		build_teacher_net=build_deep_resnet, 
		build_student_net=build_deep_plainnet, 
		root_dir=exp_dir+'nequals-3-mimic/', 
		num_teacher_epochs=85, 
		num_student_epochs=200, 
		teacher_lr=0.1, 
		student_lr=0.01)

	##########################################
	##### Experiment 5: N equals 5 Mimic #####
	##########################################

	mimic_net_experiments(
		data_dir=cifar10_dir, 
		num_trials=1, 
		stack_depth=5, 
		build_teacher_net=build_deep_resnet, 
		build_student_net=build_deep_plainnet, 
		root_dir=exp_dir+'nequals-5-mimic/', 
		num_teacher_epochs=85, 
		num_student_epochs=200, 
		teacher_lr=0.1, 
		student_lr=0.01)

	##########################################
	##### Experiment 6: N equals 7 Mimic #####
	##########################################

	mimic_net_experiments(
		data_dir=cifar10_dir, 
		num_trials=1, 
		stack_depth=7, 
		build_teacher_net=build_deep_resnet, 
		build_student_net=build_deep_plainnet, 
		root_dir=exp_dir+'nequals-7-mimic/', 
		num_teacher_epochs=85, 
		num_student_epochs=200, 
		teacher_lr=0.1, 
		student_lr=0.01)

	#############################################
	##### Experiment 7: N equals 3 LR decay #####
	#############################################

	layer_wise_lr_decay_cifar10_experiment(
		root_dir=exp_dir+'n-equals-3-lr-decay/', 
		data_dir=cifar10_dir, 
		stack_depth=3, 
		num_trials=1, 
		num_epochs=60, 
		lr_decay_epoch=40)

	#############################################
	##### Experiment 8: N equals 5 LR decay #####
	#############################################

	layer_wise_lr_decay_cifar10_experiment(
		root_dir=exp_dir+'n-equals-5-lr-decay/', 
		data_dir=cifar10_dir, 
		stack_depth=5, 
		num_trials=1, 
		num_epochs=60, 
		lr_decay_epoch=40)

	#############################################
	##### Experiment 9: N equals 7 LR decay #####
	#############################################

	layer_wise_lr_decay_cifar10_experiment(
		root_dir=exp_dir+'n-equals-7-lr-decay/', 
		data_dir=cifar10_dir, 
		stack_depth=7, 
		num_trials=1, 
		num_epochs=60, 
		lr_decay_epoch=40)