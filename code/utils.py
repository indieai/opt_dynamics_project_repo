import numpy as np
import itertools, theano

def gcn(X_train, X_val, X_test, std_normalize=True):
	""" Performs Global Contrast Normalization: each raw input feature will be transformed to have mean zero and variance 1 across the dataset. 

	Parameters
	----------

	* X_train : numpy array 
		The training set. 

	* X_val : numpy array 
		A validation set. If no validation set, can input a properly shaped 0 array. For example if X_train has shape (N, C, H, W), it suffices to input (0, C, H, W) shaped array for X_val.

	* X_test : numpy array 
		The test set. 

	* std_normalize: boolean True/False.
		If set to True, we normalize the variance of each input feature to 1, if set to False we only perform zero-centering. 
	"""

	mean_img = np.mean(X_train, axis=0)
	X_train -= mean_img 
	X_val -= mean_img
	X_test -= mean_img

	if std_normalize:
		std_img = X_train.std(axis=0)
		X_train /= std_img 
		X_val /= std_img 
		X_test /= std_img 
		return X_train, X_val, X_test, mean_img, std_img
	else:
		return X_train, X_val, X_test, mean_img

def flips(X, mode):
	""" Returns a flipped version of the input dataset: either all horiztonal or vertical flips are performed. 

	Parameters
	----------
	* X : numpy array type 
		Input dataset of shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* mode : string type 
		String for which types of flips to perform, either "horizontal" or "vertical" 
	"""
	assert(mode == "horizontal" or mode == "vertical"), "Invalid mode selected, options are: 'horizontal' or 'vertical'."

	if mode == "vertical":
		return X[:,:, ::-1, :]
	elif mode == "horizontal":
		return X[:, :, :, ::-1] 

def random_crop(X, crop_size, pad=(0,0)):
	""" Returns a random crop of each image in a dataset, along with the corresponding labels. 

	Parameters
	----------
	* X : numpy array type 
		Input dataset of shape (N, C, H, W) where:
			* N : number of data points
			* C : number of image channels 
			* H : spatial height of the image 
			* W : spatial width of the image 

	* crop_size : tuple type 
		Crop size. 

	* pad: tuple type
		Spatial padding. Default is set to (0,0), i.e. no padding.
	"""
	# Pad the data:
	if pad != (0,0):
		X = np.lib.pad(X, ((0,0),(0,0),(pad[0],pad[0]), (pad[1],pad[1])), mode='constant')

	N, C, H, W = X.shape 
	h, w = crop_size

	assert (h < H and w < W), "Crop dimensions cannot exceed original image dimensions." 

	X_random_crops = np.zeros((N,C,h,w))

	# Choose a random corner point in a vectorized fashion:
	corners = np.random.random_integers(low=0, high=H-h, size=(N,2))

	for i in xrange(N):
		# Choose a random corner point:
		# print corners[i]
		# print X[i, :, corners[i,0]:corners[i,0]+h, corners[i,1]:corners[i,1]+w].shape
		X_random_crops[i] = X[i, :, corners[i,0]:corners[i,0]+h, corners[i,1]:corners[i,1]+w]

	return X_random_crops.astype(theano.config.floatX)

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	""" A minibatch iterator. 

	Parameters
	----------
	* inputs : numpy array 
		Input dataset. 

	* targets : numpy array 
		Labels of the input dataset.

	* batchsize : int 
		An integer specifying the size of each minibatch 

	* shuffle : boolean True/False 
		True/False as to whether or not we will be shuffling the data each time we complete a full pass. 

	"""
	assert len(inputs) == len(targets)
	if shuffle:
		indices = np.arange(len(inputs))
		np.random.shuffle(indices)
	for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]

def iterate_minibatches_aux(inputs, targets, aux_targets, batchsize, shuffle=False):
	""" A minibatch iterator for inputs, targets and auxiliary targets

	Parameters
	----------
	* inputs : numpy array 
		Input dataset. 

	* targets : numpy array 
		Labels of the input dataset.

	* batchsize : int 
		An integer specifying the size of each minibatch 

	* shuffle : boolean True/False 
		True/False as to whether or not we will be shuffling the data each time we complete a full pass. 

	"""
	assert len(inputs) == len(targets)
	if shuffle:
		indices = np.arange(len(inputs))
		np.random.shuffle(indices)
	for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt], aux_targets[excerpt]


if __name__ == '__main__':
	pass