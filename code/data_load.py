import numpy as np
import cPickle as pickle
import os
import theano
import matplotlib.pyplot as plt

def cifar10_load(dir_path, num_val, resize=True, dtype_convert=True):
	""" Loads the CIFAR-10 dataset and returns it as NumPy arrays.

	Parameters
	----------

	* dir_path : string 
		A string specifying the path to the directory which contains the CIFAR-10 files. 

	* num_val : int
		An integer which specifies the size of the validation set. 

	* resize: boolean True/False
		A boolean True/False which specifies whether or not to resize all the images from (3072,) into (3,32,32) shape.

	* dtype_convert: boolean True/False
		A boolean True/False which specifies whether or not to convert the image data to 'float32' type and the labels to 'int32' type. 
	"""

	# Load and concatenate all the training batches:
	for i in range(1,6):
		batch_dict = pickle.load(open(os.path.join(dir_path, 'data_batch_%d' % (i)), 'rb'))
		if i == 1:
			X = batch_dict['data']
			Y = batch_dict['labels']
		else:
			X = np.concatenate((X, batch_dict['data']), axis = 0)
			Y = np.concatenate((Y, batch_dict['labels']), axis = 0)

	num_data = X.shape[0]

	# Create a train/val split:
	X_train = X[xrange(0, num_data-num_val)]
	Y_train = Y[0:num_data-num_val]

	X_val = X[xrange(num_data-num_val, num_data)]
	Y_val = Y[num_data-num_val:num_data]

	del X, Y

	# Load the test dataset
	test_dict = pickle.load(open(os.path.join(dir_path, 'test_batch'), 'rb'))
	X_test = np.array(test_dict['data'])
	Y_test = np.array(test_dict['labels'])

	if resize:
		X_train = X_train.reshape(num_data-num_val, 3, 32, 32)
		X_val = X_val.reshape(num_val, 3, 32, 32)
		X_test = X_test.reshape(X_test.shape[0], 3, 32, 32)

	if dtype_convert:
		X_train = X_train.astype('float32')
		Y_train = Y_train.astype('int32')
		X_val = X_val.astype('float32')
		Y_val = Y_val.astype('int32')
		X_test = X_test.astype('float32')
		Y_test = Y_test.astype('int32')

	return X_train, Y_train, X_val, Y_val, X_test, Y_test

def pentomino_load(dir_path, mode, num_train, num_val, binary_labels=True):
	""" Loads the Pentomino dataset and returns a dictionary type. 

	Parameters
	----------

	* dir_path : string type
		A string type specifying the path to the directory containing the Pentomino data files.

	* mode : string 
		A string type specifying which Pentomino set to load. Options are: '10k', '20k', '40k', '80k'

	* num_train: int 
		An integer specifying the size of the train set. 

	* num_val : int 
		An integer specifying the size of the validation set.

	* binary_labels : boolean True/False
		A boolean True/False specifying whether or not to return the binary labels or not. Default is set to True.

	"""
	
	if mode == '10k':
		pentomino = np.load(open(os.path.join(dir_path,'pento64x64_10k_seed_23111298122_64patches.npy'), 'rb'))
		num_data = 10000
	elif mode == '20k':
		pentomino = pickle.load(open(os.path.join(dir_path, 'pento64x64_20k_64patches_seed_112168712_64patches.pkl'), 'rb'))
		num_data = 20000
	elif mode == '40k':
		pentomino = np.load(open(os.path.join(dir_path, 'pento64x64_40k_64patches_seed_975168712_64patches.npy'), 'rb'))
		num_data = 40000 
	elif mode == '80k':
		pentomino = np.load(open(os.path.join(dir_path, 'pento64x64_80k_64patches_seed_735128712_64patches.npy'), 'rb')) 
		num_data = 80000
	else:
		# Raise a value error 
		pass # Add it in later! 

	if mode=='10k' or mode=='40k' or mode=='80k':
		# Perform numpy serialized object load 
		y = pentomino[1,:] 
		if binary_labels:
			diff_idx = np.nonzero(y != 10)[0]
			Y = np.ones((num_data))
			Y[diff_idx] = 0
		else:
			Y = np.array([y[i][0] for i in range(len(y))])

		# print "Num of labels: %d" % (np.unique(Y).size)
		Y_train = Y[range(num_train)].astype('int32')
		Y_val = Y[range(num_train, num_train+num_val)].astype('int32')
		Y_test = Y[range(num_train+num_val, num_data)].astype('int32')

		X_train = np.array([pentomino[:,i][0] for i in xrange(num_train)])
		X_val = np.array([pentomino[:,i][0] for i in xrange(num_train, num_train+num_val)])
		X_test = np.array([pentomino[:,i][0] for i in xrange(num_train+num_val, num_data)])

		# Reshape the data:
		X_train = X_train.reshape(-1, 1, 64, 64)
		X_val = X_val.reshape(-1, 1, 64, 64)
		X_test = X_test.reshape(-1, 1, 64, 64)

	elif mode == '20k':
		# Perform pickle serial load

		y = pentomino[1]
		if binary_labels:
			diff_idx = np.nonzero(y != 10)[0]
			Y = np.ones((num_data))
			Y[diff_idx] = 0
		else:
			Y = y

		# print "Num of labels: %d" % (np.unique(Y).size)
		Y_train = Y[range(num_train)].astype('int32')
		Y_val = Y[range(num_train, num_train+num_val)].astype('int32')
		Y_test = Y[range(num_train+num_val, num_data)].astype('int32')

		X_train = np.array([pentomino[0][i] for i in xrange(num_train)])
		X_test = np.array([pentomino[:,i][0] for i in xrange(num_train+num_val, num_data)])
		X_test = np.array([pentomino[0][i] for i in xrange(num_train+num_val, num_data)])

		# Reshape the data:
		X_train = X_train.reshape(-1, 1, 64, 64)
		X_val = X_val.reshape(-1, 1, 64, 64)
		X_test = X_test.reshape(-1, 1, 64, 64)

	return X_train, Y_train, X_val, Y_val, X_test, Y_test

if __name__ == '__main__':
	pass