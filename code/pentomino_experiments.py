import theano, lasagne, os, time 
import theano.tensor as T
import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from collections import OrderedDict
from lasagne.layers import get_all_param_values, set_all_param_values
from lasagne.layers import count_params, get_output
from tsne import bh_sne
from modules import adadelta_trainer, compute_activation_statistics
from utils import gcn, flips, random_crop, iterate_minibatches
from data_load import pentomino_load, cifar10_load
from networks import build_pentomino_deep_vgg_resnet
from networks import build_pentomino_deep_vgg_plainnet
from networks import build_pentomino_deep_inception_resnet
from networks import build_pentomino_deep_inception_plainnet

def train_net_n_trials(build_net, root_dir, dir_path, stack_depth, num_trials=10, pentomino_mode='40k', num_train=32000, num_val=0, binary_labels=True,  save_iter=100, num_epochs=100, batchsize=100, lr=1.0, reg=0.0):
	""" Trains multiple randomly initialized networks using the ADADELTA trainer.

	Parameters
	----------

	* build_net : function type 
		A build network function.

	* root_dir : string
		A string specifying the root directory.

	* dir_path : string type
		A string type specifying the path to the directory containing the Pentomino data files.

	* stack_depth : int 
		An integer specifying the stack depth of the blocks in the network. 

	* num_trials : int 
		An integer specifying the number of trials to perform for each (N, learning rate) pair. 

	* pentomino_mode : string 
		A string type specifying which Pentomino set to load. Options are: '10k', '20k', '40k', '80k'. Default is set to 40k.

	* num_train: int 
		An integer specifying the size of the train set. Default is set to 32K

	* num_val : int 
		An integer specifying the size of the validation set. Default is set to 0.

	* binary_labels : boolean True/False
		A boolean True/False specifying whether or not to return the binary labels or not. Default is set to True.

	* save_iter : int 
		An integer specifying how many training batch updates to perform before we save the weights and evaluate on the test set. Default is set to 100.

	* num_epochs : int 
		An integer specifying the number of training epochs. Default is set to 100. 

	* batchsize : int 
		An integer specifying the size of each training batch. Default is set to 100.

	* lr: float 
		A float specifying the learning rate. Default is set to 1.0.

	* reg : float 
		A float specifying the l2 regularization parameter value. Default is set to 0.
	"""

	# Create symbolic input and target variables:
	input_var = T.tensor4('inputs')
	target_var = T.ivector('targets')

	# Load the Pentomino dataset:
	X_train, Y_train, X_val, Y_val, X_test, Y_test = pentomino_load(dir_path=dir_path, mode=pentomino_mode, num_train=num_train, num_val=num_val)

	# Zero center the data:
	X_train, X_val, X_test, mean_img = gcn(X_train, X_val, X_test, False)

	# Prepare a data dictionary:
	data_dict = OrderedDict()
	data_dict['X_train'] = X_train
	data_dict['Y_train'] = Y_train
	data_dict['X_test'] = X_test 
	data_dict['Y_test'] = Y_test

	if build_net == build_pentomino_deep_vgg_resnet:
		save_dir = root_dir+'resnet/stack_depth_%d/lr_%d/' % (stack_depth, int(lr))
	elif build_net == build_pentomino_deep_vgg_plainnet:
		save_dir = root_dir+'vgg_plain/stack_depth_%d/lr_%d/' % (stack_depth, int(lr))
	else:
		save_dir = root_dir

	for i in xrange(num_trials):
		print "Performing trial %d of %d" % (i+1, num_trials)
		if i == 0:
			start_time = time.time()

		if os.path.isfile(save_dir+'trial_%d/train_snapshot.pkl' % (i+1)) == True:
			print "Passed on Trial %d" % (i+1)
		else:
			# Initialize a network:
			net = build_net(input_var=input_var, stack_depth=stack_depth)

			train_snapshot = adadelta_trainer(
								net=net, 
								input_var=input_var, 
								target_var=target_var, 
								data_dict=data_dict, 
								lr=lr,
								reg=reg,
								save_iter=save_iter,
								batchsize=batchsize,
								num_epochs=num_epochs, 
								save_dir=save_dir+'trial_%d/' % (i+1)
								)
			if i == 0:
				end_time = time.time() - start_time
				print "\tFirst trial took %f seconds." % (end_time)

def train_models_stride_s_n_trials(root_dir, dir_path, first_stride=2, num_trials=5, pentomino_mode='40k', num_train=32000, num_val=0, binary_labels=True, save_iter=100, num_epochs=100, batchsize=100, lr=1.0, reg=0.0):
	""" Trains ResNet-N-S and PlainNet-N-S models for N equals 2,3 and 4 with ADADELTA and variable first convolutional stride S. We train 5 randomly initialized models for each configuration.

	Parameters
	----------

	* root_dir : string
		A string specifying the root directory. Will have the following hierarchy:
			- graphics/
			- resnet/
				- stack_depth_2/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_3/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_4/
					- trial_1/
					- .
					- .
					- trial_5/
			- plainnet/
				- stack_depth_2/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_3/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_4/
					- trial_1/
					- .
					- .
					- trial_5/

	* dir_path : string type
		A string type specifying the path to the directory containing the Pentomino data files. Default is set to the PC Pentomino directory. 

	* first_stride : int 
		An integer specifying the length of the first convolutional stride. Default is set to 2.

	* num_trials : int 
		An integer specifying the number of networks to train for each configuration. Default is set to 5.

	* pentomino_mode : string 
		A string type specifying which Pentomino set to load. Options are: '10k', '20k', '40k', '80k'. Default is set to 40k.

	* num_train: int 
		An integer specifying the size of the train set. Default is set to 32K.

	* num_val : int 
		An integer specifying the size of the validation set. Default is set to 0.

	* binary_labels : boolean True/False
		A boolean True/False specifying whether or not to return the binary labels or not. Default is set to True.

	* save_iter : int 
		An integer specifying how many training batch updates to perform before we save the weights and evaluate on the test set. Default is set to 100.

	* num_epochs : int 
		An integer specifying the number of training epochs. Default is set to 100. 

	* batchsize : int 
		An integer specifying the size of each training batch. Default is set to 100.

	* lr: float 
		A float specifying the learning rate. Default is set to 1.0.

	* reg : float 
		A float specifying the l2 regularization parameter value. Default is set to 0.
	"""

	# Create the directory structure in root_dir:
	if os.path.exists(root_dir) == False:
		os.makedirs(root_dir)

	if os.path.exists(root_dir+'graphics/') == False:
		os.makedirs(root_dir+'graphics/')

	stack_depths = [2,3,4]

	for model in ['plainnet/', 'resnet/']:
		if os.path.exists(root_dir+model)==False:
			os.makedirs(root_dir+model)
		for stack_depth in ['stack_depth_%d/' % (i) for i in stack_depths]:
			if os.path.exists(root_dir+model+stack_depth) == False:
				os.makedirs(root_dir+model+stack_depth)
			for trial in ['trial_%d/' % (i) for i in xrange(1, num_trials+1)]:
				if os.path.exists(root_dir+model+stack_depth+trial) == False:
					os.makedirs(root_dir+model+stack_depth+trial)

	def build_vgg_res_stride_s(input_var, stack_depth):
		return build_pentomino_deep_vgg_resnet(input_var=input_var, stack_depth=stack_depth, first_stride=first_stride)

	def build_vgg_plain_stride_s(input_var, stack_depth):
		return build_pentomino_deep_vgg_plainnet(input_var=input_var, stack_depth=stack_depth, first_stride=first_stride)

	for n in stack_depths:
	
		# Set the batchsize:
		if n == 2 or n == 3:
			batchsize = 100
		elif n == 4:
			batchsize = 80

		print "Performing ResNet-%d-S%d training" % (n, first_stride)
		np.random.seed(123456)
		train_net_n_trials(
			build_net=build_vgg_res_stride_s, 
			stack_depth=n, 
			num_trials=num_trials, 
			root_dir=root_dir+'resnet/stack_depth_%d/' % (n), 
			pentomino_mode=pentomino_mode, 
			num_train=num_train, 
			num_val=num_val, 
			binary_labels=binary_labels, 
			dir_path=dir_path, 
			save_iter=save_iter, 
			num_epochs=num_epochs, 
			batchsize=batchsize, 
			lr=lr, 
			reg=reg)

		print "Performing PlainNet-%d-S%d training" % (n, first_stride)
		np.random.seed(123456)
		train_net_n_trials(
			build_net=build_vgg_plain_stride_s, 
			stack_depth=n, 
			num_trials=num_trials, 
			root_dir=root_dir+'plainnet/stack_depth_%d/' % (n), 
			pentomino_mode=pentomino_mode, 
			num_train=num_train, 
			num_val=num_val, 
			binary_labels=binary_labels, 
			dir_path=dir_path, 
			save_iter=save_iter, 
			num_epochs=num_epochs, 
			batchsize=batchsize, 
			lr=lr, 
			reg=reg)

def pentomino_stride_s_n_trials_plots(root_dir, first_stride=2, num_trials=5):
	""" Computes all the Pentomino experimental plots. 

	Parameters
	----------

	* root_dir : string
		A string specifying the root directory. Will have the following hierarchy:
			- graphics/
			- resnet/
				- stack_depth_2/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_3/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_4/
					- trial_1/
					- .
					- .
					- trial_5/
			- plainnet/
				- stack_depth_2/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_3/
					- trial_1/
					- .
					- .
					- trial_5/
				- stack_depth_4/
					- trial_1/
					- .
					- .
					- trial_5/

	* first_stride : int 
		An integer specifying the length of the first convolutional stride. Default is set to 2.

	* num_trials : int 
		An integer specifying the number of networks that were trained for each configuration. Default is set to 5.

	"""
	stack_depths = [2, 3, 4]
	trials = np.arange(1,num_trials+1)

	res_auc_dict = OrderedDict()
	plain_auc_dict = OrderedDict()
	for stack_depth in stack_depths:
		res_auc_dict['stack_depth_%d' % (stack_depth)] = OrderedDict()
		plain_auc_dict['stack_depth_%d' % (stack_depth)] = OrderedDict()
		
		res_auc_dict['stack_depth_%d' % (stack_depth)] = np.zeros(num_trials)
		plain_auc_dict['stack_depth_%d' % (stack_depth)] = np.zeros(num_trials)

	# For each stack_depth and lr, create an AUC barplot:
	for stack_depth in stack_depths:
		for index, trial in enumerate(trials):
			res_path = root_dir+'resnet/stack_depth_%d/trial_%d/' % (stack_depth, trial)
			plain_path = root_dir+'plainnet/stack_depth_%d/trial_%d/' % (stack_depth, trial)
			res_dict = pickle.load(open(res_path+'train_snapshot.pkl', 'rb'))
			plain_dict = pickle.load(open(plain_path+'train_snapshot.pkl', 'rb'))

			res_auc_dict['stack_depth_%d' % (stack_depth)][index] = np.trapz(res_dict['train_loss_epoch_arr'])
			plain_auc_dict['stack_depth_%d' % (stack_depth)][index] = np.trapz(plain_dict['train_loss_epoch_arr'])


	res_vals = [res_auc_dict['stack_depth_%d' % (stack_depth)].mean() for stack_depth in stack_depths]
	res_std = [res_auc_dict['stack_depth_%d' % (stack_depth)].std() for stack_depth in stack_depths]

	plain_vals = [plain_auc_dict['stack_depth_%d' % (stack_depth)].mean() for stack_depth in stack_depths]
	plain_std = [plain_auc_dict['stack_depth_%d' % (stack_depth)].std() for stack_depth in stack_depths]

	ind = np.arange(len(stack_depths))

	width = 0.35 
	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, res_vals, width, color='r', yerr=res_std)
	rects2 = ax.bar(ind+width, plain_vals, width, color='b', yerr=plain_std)
	ax.set_ylabel('Area Under Curve')
	ax.set_title('Training Per Epoch AUC vs. Stack Depth\n Averaged Over %d trials' % (num_trials))
	ax.set_xticks(ind + width)
	ax.set_xticklabels(('Stack Depth 2-Stride-%d' % (first_stride), 'Stack Depth 3-Stride-%d' % (first_stride), 'Stack Depth 4-Stride-%d' % (first_stride)))
	ax.legend((rects1[0], rects2[0]), ('Res', 'Plain'), loc='upper center')
	plt.tight_layout()
	plt.savefig(root_dir+'graphics/vgg_auc_bar_plot_.png')
	plt.close()

	# Create dense mean train and test error plots:
	error_diagnostics = ['train_loss_epoch_arr', 'test_acc_arr', 'test_loss_epoch_arr', 'test_acc_epoch_arr']
	res_best_test_acc = OrderedDict()
	plain_best_test_acc = OrderedDict()
	for i in [2,3,4]:
		res_best_test_acc['stack_depth_%d' % (i)] = OrderedDict()
		plain_best_test_acc['stack_depth_%d' % (i)] = OrderedDict()

		res_best_test_acc['stack_depth_%d' % (i)] = 0.0
		plain_best_test_acc['stack_depth_%d' % (i)] = 0.0

	for stack_depth in stack_depths:
		for error in error_diagnostics:
			for trial in xrange(num_trials):
				# Load the res and plain dicts:
				res_path = root_dir+'resnet/stack_depth_%d/trial_%d/' % (stack_depth, trial+1)
				plain_path = root_dir+'plainnet/stack_depth_%d/trial_%d/' % (stack_depth, trial+1)
				res_dict = pickle.load(open(res_path+'train_snapshot.pkl', 'rb'))
				plain_dict = pickle.load(open(plain_path+'train_snapshot.pkl', 'rb'))

				if error == 'test_acc_arr':
					if res_dict[error].max() > res_best_test_acc['stack_depth_%d' % (stack_depth)]:
						res_best_test_acc['stack_depth_%d' % (stack_depth)] = res_dict[error].max()

					if plain_dict[error].max() > plain_best_test_acc['stack_depth_%d' % (stack_depth)]:
						plain_best_test_acc['stack_depth_%d' % (stack_depth)] = plain_dict[error].max()

				if trial == 0:
					N = res_dict[error].shape[0]

					res_vals = np.zeros((num_trials, N))
					plain_vals = np.zeros((num_trials, N))

				res_vals[trial] = res_dict[error]
				plain_vals[trial] = plain_dict[error]

			# Compute mean and std vals:
			res_mean, res_std = res_vals.mean(axis=0), res_vals.std(axis=0)
			plain_mean, plain_std = plain_vals.mean(axis=0), plain_vals.std(axis=0)

			# Create a plot:
			ind = np.arange(1, N+1)
			if 'acc' in error:
				plt.plot(ind, 100*res_mean, 'k', label='Res', color='#CC4F1B')
				plt.fill_between(ind, 100*res_mean-100*res_std, 100*res_mean+100*res_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
				plt.plot(ind, 100*plain_mean, 'k',label='Plain',  color='#1B2ACC')
				plt.fill_between(ind, 100*plain_mean-100*plain_std, 100*plain_mean+100*plain_std, alpha=0.2, edgecolor='#1B2ACC', facecolor='#089FFF', linewidth=4, linestyle='dashdot', antialiased=True)
			else:
				plt.plot(ind, res_mean, 'k', label='Res', color='#CC4F1B')
				plt.fill_between(ind, res_mean-res_std, res_mean+res_std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
				plt.plot(ind, plain_mean, 'k',label='Plain',  color='#1B2ACC')
				plt.fill_between(ind, plain_mean-plain_std, plain_mean+plain_std, alpha=0.2, edgecolor='#1B2ACC', facecolor='#089FFF', linewidth=4, linestyle='dashdot', antialiased=True)
			if 'epoch' in error:
				plt.xlabel('Epoch Number')
			else:
				plt.xlabel('Batch Number')
			if 'acc' in error:
				plt.ylabel('Accuracy %')
			else:
				plt.ylabel('Loss')

			if error == 'train_loss_epoch_arr':
				plt.title('Training Loss Per Epoch Averaged Over %d trials\nStack Depth: %d, Stride: %d' % (num_trials, stack_depth, first_stride))
			elif error == 'test_acc_arr':
				plt.title('Test Acc Per Batch Averaged Over %d trials\nStack Depth: %d, Stride: %d' % (num_trials, stack_depth, first_stride))
			elif error == 'test_loss_epoch_arr':
				plt.title('Test Loss Per Epoch Averaged Over %d trials\nStack Depth: %d, Stride: %d' % (num_trials, stack_depth, first_stride))
			elif error == 'test_acc_epoch_arr':
				plt.title('Test Acc Per Epoch Averaged Over %d trials\nStack Depth: %d, Stride: %d' % (num_trials, stack_depth, first_stride))
			plt.tight_layout()
			if 'acc' in error:
				plt.legend(loc='lower right')
			else:
				plt.legend(loc='upper right')
			plt.savefig(root_dir+'graphics/stack_depth_%d_%s_mean_dense_err.png' % (stack_depth, error))
			plt.close()

	# Show the existence of saddles in the optimization landscape:
	for stack_depth in stack_depths:
		for trial in xrange(num_trials):
			# Load the res and plain dicts:
			res_path = root_dir+'resnet/stack_depth_%d/trial_%d/' % (stack_depth, trial+1)
			plain_path = root_dir+'plainnet/stack_depth_%d/trial_%d/' % (stack_depth, trial+1)
			res_dict = pickle.load(open(res_path+'train_snapshot.pkl', 'rb'))
			plain_dict = pickle.load(open(plain_path+'train_snapshot.pkl', 'rb'))

			if trial == 0:
				N = res_dict['train_loss_epoch_arr'].shape[0]

				res_vals = np.zeros((num_trials, N))
				plain_vals = np.zeros((num_trials, N))

			res_vals[trial] = res_dict['train_loss_epoch_arr']
			plain_vals[trial] = plain_dict['train_loss_epoch_arr']

		# Make Residual plots:
		colormap = plt.cm.jet
		colors = [colormap(i) for i in np.linspace(0, 1,10)]
		for i in xrange(num_trials):
			plt.plot(res_vals[i], color=colors[i], label='Trial %d' % (i+1))
		plt.legend(loc='upper right')
		plt.title('ResNet-%d-S%d: Train Loss Per Epoch over %d Trials' % (stack_depth, first_stride, num_trials))
		plt.xlabel('Epoch Number')
		plt.ylabel('Loss')
		plt.tight_layout()
		plt.savefig(root_dir+'graphics/res_stack_depth_%d_train_curves.png' % (stack_depth))
		plt.close()

		# Make Plain plots:
		for i in xrange(num_trials):
			plt.plot(plain_vals[i], color=colors[i], label='Trial %d' % (i+1))
		plt.legend(loc='upper right')
		plt.title('PlainNet-%d-S%d: Train Loss Per Epoch over %d Trials' % (stack_depth, first_stride, num_trials))
		plt.xlabel('Epoch Number')
		plt.ylabel('Loss')
		plt.tight_layout()
		plt.savefig(root_dir+'graphics/plain_stack_depth_%d_train_curves.png' % (stack_depth))
		plt.close()

def raw_tsne_plots(mnist_dir, cifar_10_dir, pentomino_dir, save_dir, N=10000):
	""" Produces t-SNE embedding plots for the MNIST, CIFAR-10 and Pentomino datasets.

	Parameters
	----------

	* mnist_dir : string
		A string specifying the file path to the MNIST pickle file. 

	* cifar_10_dir: string 
		A string specifying the file path to the CIFAR-10 dataset. 

	* pentomino_dir : string 
		A string specifying the file path to the Pentomino 40K dataset. 

	* save_dir : string 
		A string specifying where to save all the t-SNE plots. 

	* N : int 
		An integer specifying the number of datapoints to use in our t-SNE embedding.

	"""
	
	####################################
	##### Create MNIST t-SNE plots #####
	####################################
	print "Performing MNIST t-SNE..."
	# Load the MNIST
	mnist = pickle.load(open(mnist_dir+'mnist.pkl', 'rb'))
	X_train, Y_train = mnist[0][0].astype('float64'), mnist[0][1].astype('int32')
	X, Y = X_train[0:N], Y_train[0:N]

	# Perform the t-SNE embedding:
	vis_data = bh_sne(X)

	# plot the result
	vis_x = vis_data[:, 0]
	vis_y = vis_data[:, 1]

	plt.scatter(vis_x, vis_y, c=Y, cmap=plt.cm.get_cmap("jet", 10))
	plt.colorbar(ticks=range(10))
	plt.clim(-0.5, 9.5)
	plt.title('MNIST 10K Datapoint t-SNE')
	plt.tight_layout()
	plt.savefig(save_dir+'mnist_tsne.png')
	plt.close()

	#######################################
	##### Create CIFAR-10 t-SNE plots #####
	#######################################
	print "Performing CIFAR-10 t-SNE..."
	# Load the CIFAR-10 data
	X_train, Y_train, X_val, Y_val, X_test, Y_test = cifar10_load(dir_path=cifar_10_dir, num_val=0) 
	X, Y = X_train[0:N].reshape(N,-1).astype('float64'), Y_train[0:N]

	# Perform the t-SNE embedding:
	vis_data = bh_sne(X)

	# plot the result
	vis_x = vis_data[:, 0]
	vis_y = vis_data[:, 1]

	plt.scatter(vis_x, vis_y, c=Y, cmap=plt.cm.get_cmap("jet", 10))
	plt.colorbar(ticks=range(10))
	plt.clim(-0.5, 9.5)
	plt.title('CIFAR-10 10K Datapoint t-SNE')
	plt.tight_layout()
	plt.savefig(save_dir+'cifar10_tsne.png')
	plt.close()

	########################################
	##### Create Pentomino t-SNE plots #####
	########################################
	print "Performing Pentomino t-SNE..."
	X_train, Y_train, X_val, Y_val, X_test, Y_test = pentomino_load(mode='40k', num_train=32000, num_val=0, dir_path=pentomino_dir)
	X, Y = X_train[0:N].reshape(N,-1).astype('float64'), Y_train[0:N]

	# Perform the t-SNE embedding:
	vis_data = bh_sne(X)

	# plot the result
	vis_x = vis_data[:, 0]
	vis_y = vis_data[:, 1]

	plt.scatter(vis_x, vis_y, c=Y, cmap=plt.cm.get_cmap("jet", 2))
	plt.colorbar(ticks=range(2))
	plt.clim(-0.5, 1.5)
	plt.title('Pentomino 10K Datapoint t-SNE')
	plt.tight_layout()
	# plt.savefig('pentomino_tsne.png')
	plt.show()
	plt.close()

if __name__ == '__main__':

	##############################
	##### Define directories #####
	##############################
	
	exp_dir 		= '/your_experiment_directory/'
	pentomino_dir 	= '/your_pentomino_directory/'
	mnist_dir 		= '/your_mnist_directory/'
	cifar10_dir 	= '/your_cifar_10_directory/'
	tsne_save_dir 	= '/your_tsne_save_dir/'

	###########################################
	##### Experiment 1: Stride 1 Networks #####
	###########################################

	# Train 5/5 VGG-Res-N, VGG-Plain-N models 
	train_models_stride_s_n_trials(
		root_dir=exp_dir+'pentomino_stride_1_200_epochs_experiments/',
		dir_path=pentomino_dir,
		num_epochs=200, 
		num_trials=5, 
		first_stride=1)
	
	# Create Plots
	pentomino_stride_s_n_trials_plots(
		root_dir=exp_dir+'pentomino_stride_1_200_epochs_experiments/', 
		num_trials=5, 
		first_stride=1)

	###########################################
	##### Experiment 2: Stride 2 Networks #####
	###########################################

	# Train 5/5 VGG-Res-N, VGG-Plain-N models 
	train_models_stride_s_n_trials(
		root_dir=exp_dir+'pentomino_stride_2_100_epochs_experiments/',
		dir_path=pentomino_dir,
		num_epochs=100, 
		num_trials=5, 
		first_stride=2)
	
	# Create Plots
	pentomino_stride_s_n_trials_plots(
		root_dir=exp_dir+'pentomino_stride_2_100_epochs_experiments/', 
		num_trials=5, 
		first_stride=2)

	#####################################
	##### Experiment 3: t-SNE plots #####
	#####################################

	# Raw feature input t-SNE:
	raw_tsne_plots(
		mnist_dir=mnist_dir, 
		cifar_10_dir=cifar10_dir, 
		pentomino_dir=pentomino_dir, 
		save_dir=tsne_save_dir, 
		N=10000)